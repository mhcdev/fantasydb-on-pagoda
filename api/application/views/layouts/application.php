<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Bootstrap, from Twitter</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/bootstrap-colorpicker.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/jquery.dataTables.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/imperio.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/bootstrap-editable.css" rel="stylesheet">
  <style>
  body {
    padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
  }
  </style>
  <link href="<?php echo base_url();?>assets/css/bootstrap-responsive.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap-colorpicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap-editable.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/main.js"></script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
      <![endif]-->

      <!-- Fav and touch icons -->

    </head>

    <body>

      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="brand" href="#">Fantasy DB</a>
            <div class="nav-collapse collapse">
              <ul class="nav">
                <li class="active"><a href="#">Dashboard</a></li>
                <li><a href="<?php echo site_url( 'team' );?>">Teams</a>
                <li><a href="<?php echo site_url( 'player' );?>">Players</a>
                <li><a href="<?php echo site_url( 'position' );?>">Positions</a>
               <li><a href="<?php echo site_url( 'stat' );?>">Stats</a>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </div>
      </div>

          <div class="container">

            <?php echo $yield;?>

          </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

  </body>
  </html>
