<?php if (isset($viewing_year)):?>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <?php echo $team_info->short_name;?>  -- <?php echo $viewing_year;?>

                </div>
                <div class="box-content nopadding">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>Position</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Full Name</th>
                                <th>Position</th>
                            </tr>
                        </tfoot>
                        <tbody id="pages_index">
                            <?php foreach ($players as $i => $player):?>
                            <tr class="" id="row_<?php echo $player->id;?>">
                                <td><a href="<?php echo site_url('player/view/'.$player->player_id);?>"><?php echo $player->player_info->full_name;?></a></td>
                                <td><?php echo FR_lookup_id('position_model', $player->player_info->position, 'name').' '.$player->pos;?></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php else:?>
    <?php foreach ($years as $key => $years):?>

     <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    <?php echo $team_info->short_name;?>  -- <?php echo $key;?>

                </div>
                <div class="box-content nopadding">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>Position</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Full Name</th>
                                <th>Position</th>
                            </tr>
                        </tfoot>
                        <tbody id="pages_index">
                            <?php foreach ($years as $i => $year):?>
                            <tr class="" id="row_<?php echo $year->id;?>">
                                <td><a href="<?php echo site_url('player/view/'.$year->player_id);?>"><?php echo $year->player_info->first_name.' '.$year->player_info->last_name;?></a></td>
                                <td><?php echo $year->pos;?></td>

                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>   


<?php endforeach;?>
<?php endif;?>