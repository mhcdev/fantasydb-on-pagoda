<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                Positions
                <?php /*
                <a href="<?php echo site_url('stat/add');?>" class="btn btn-success btn-mini pull-right">Add Stat</a>
*/?>
</div>
<div class="box-content nopadding">
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Name</th>
                <th style="width:210px;" >&nbsp;</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Name</th>
                <th style="width:210px;" >&nbsp;</th>
            </tr>
        </tfoot>
        <tbody id="pages_index">
            <?php foreach ($positions as $i => $position):?>
            <tr class="" id="row_<?php echo $position->id;?>">
                <td><a href="<?php echo site_url('position/view/'.$position->id);?>"><?php echo $position->name;?></a></td>
                     <!--
                        <td class="center"> 4</td>
                        <td class="center">X</td>
                    -->
                    <td class="">
                        <a href="<?php echo site_url('position/edit/'.$position->id);?>" class="btn btn-info btn-mini">Edit</a>
                        <a href="#" class="btn btn-danger btn-mini delete">Delete</a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
</div>
</div>
</div>
