<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                Players
                <a href="<?php echo site_url('player/add');?>" class="btn btn-success btn-mini pull-right">Add Player</a>

            </div>
            <div class="box-content nopadding">
                <table id="" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Position</th>
                            <th>Fantasy Points</th>
                            <th style="width:210px;" >&nbsp;</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Position</th>
                            <th>Fantasy Points</th>
                            <th style="width:210px;" >&nbsp;</th>
                        </tr>
                    </tfoot>
                    <tbody id="pages_index">
                        <?php foreach ($players as $i => $player):?>
                        <tr class="" id="row_<?php echo $player->id;?>">
                            <td><?php echo $player->first_name;?></td>
                            <td><?php echo $player->last_name;?></td>
                            <td><?php echo FR_lookup_id('position_model', $player->position, 'name');?></td>
                            <td><?php echo $player->fp;?></td>
                     <!--
                        <td class="center"> 4</td>
                        <td class="center">X</td>
                    -->
                    <td class="">
                        <a href="<?php echo site_url('player/view/'.$player->id);?>" class="btn btn-success btn-mini">View</a>
                        <a href="<?php echo site_url('player/edit/'.$player->id);?>" class="btn btn-info btn-mini">Edit</a>
                        <a href="#" class="btn btn-danger btn-mini delete">Delete</a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
</div>
</div>
</div>
