<div class="row-fluid">
                        <div class="span12">
                            <div class="box">
                                <div class="box-title">
                                    Player Info
                            <a href="<?php echo site_url('player/edit/'.$player_info->id);?>" class="btn btn-success btn-mini pull-right">Edit Player</a>

                                </div>
                                <div class="box-content" style="padding-top:0px;">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <h3><?php echo $player_info->first_name.' '.$player_info->last_name;?> (<?php echo $player_info->rank;?>)</h3>
                                                <dl class="dl-horizontal">
                                                    <dt>Position</dt>
                                                    <dd><?php echo FR_lookup_id('position_model', $player_info->position, 'name');?>
                                                    <dt>Age</dt>
                                                    <dd><?php echo $player_info->age;?></dd>
                                                    <dt>Bye Week</dt>
                                                    <dd><?php echo (strlen($player_info->bye_week)>0) ? $player_info->bye_week : '&nbsp;';?></dd>
                                                    <dt>Profile</dt>
                                                    <dd><?php echo $player_info->profile;?></dd>
                                                </dl>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                STATS
            </div>
            <div class="box-content nopadding">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Year</th>
                            <th>Team</th>
                            <th>Pos</th>
                            <?php foreach ($avail_stats as $stat_id => $stat):?>
                            <th><?php echo $stat['name'];?></th>
                        <?php endforeach;?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Year</th>
                        <th>Team</th>
                        <th>Pos</th>
                        <?php foreach ($avail_stats as $stat_id => $stat):?>
                        <th><?php echo $stat['name'];?></th>
                    <?php endforeach;?>
                </tr>
            </tfoot>
            <tbody id="pages_index">
                <?php foreach ($player_yearly_info as $i => $year):?>
                <tr class="" id="row_<?php echo $year->id;?>">
                    <td><?php echo $year->year;?></td>
                    <td><a href="<?php echo site_url('team/view/'.$year->team_id);?>"><?php echo $year->team_info->short_name;?></a></td>
                    <td><?php echo $year->pos;?></td>
                    <?php foreach ($avail_stats as $stat_id => $stat):?>
                    <td><?php echo (isset($year->stats[$stat_id])) ? $year->stats[$stat_id] : 'NA';?></td>
                <?php endforeach;?>
            </tr>
        <?php endforeach;?>
        <tr>
            <td>2013</td>
            <td colspan=2>Projections</td>
            <?php foreach ($avail_stats as $stat_id => $stat):?>
            <td><?php echo (isset($projections[$stat_id])) ? $projections[$stat_id] : 'NA';?></td>
        <?php endforeach;?>
    </tr>
</tbody>
</table>
</div>
</div>
</div>
</div>


<?php //var_dump($avail_stats);?>
<div class="row-fluid">
    <div class="span4">
        <form class="form-horizontal" method="post" action="<?php echo current_url();?>">
           <?php echo validation_errors(); ?>
           <div class="box">
            <div class="box-title">
                Projection Stat Multipliers

            </div>

            <div class="box-content nopadding">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td>All</td>
                            <td><input type="text" class="span12" name="all_value" id="" value="" /></td>
                            <td><input type="submit" value="Update All" name="update_all" class="btn span12"/></td>
                        </tr>
                        <tr>
                            <th>Stat</th>
                            <th colspan=2>Multiplier</th>
                            <?php /*<th>&nbsp;</th>*/?>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Stat</th>
                            <th colspan=2>Multiplier</th>
                            <?php /*<th>&nbsp;</th>*/?>
                        </tr>
                    </tfoot>
                    <tbody id="pages_index">
                        <?php foreach ($avail_stats as $stat_id => $stat):?>
                        <?php if ($stat['projection'] == 'projection_formula') :?>
                        <tr class="" id="row_<?php echo $stat_id;?>">
                            <td><?php echo $stat['name'];?></td>
                            <td colspan=2><input type="text" class="span12" name="stat[<?php echo $stat_id;?>]" id="stat_<?php echo $stat_id;?>" value="<?php echo set_value('stat['.$stat_id.']',$stat['multi'])?>"/></td>
                            <?php //<td><button class="btn btn-mini" type="button">UPDATE</button></td> ?>
                        </tr>
                    <?php endif;?>
                <?php endforeach;?>   
                <tr><td colspan=3 style="text-align: right;">
                    <input type="hidden" name="player_id" value="<?php echo $player_info->id;?>" />
                    <input type="hidden" name="position_id" value="<?php echo $player_info->position;?>" />
                    <input type="submit" value="Update" class="btn span12" name="update_singles" />
                </td></tr>    
                
            </tbody>
        </table>
    </div>
</div>
</div>


<div class="span8">
    <form class="form" method="post" action="<?php echo current_url();?>">
       <?php //echo validation_errors(); ?>
       <div class="box">
        <div class="box-title">
            Fantasy Point Equation Multipliers
        </div>

        <div class="box-content ">
            <table>
                <tr>
                    <td>
                        <?php if ($player_info->position == 3):?>
                        ((<?php echo $avail_stats[$fantasy_point_stats[0]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[0]" value="<?php echo $fantasy_point_factors[0];?>">) + 
                        (<?php echo $avail_stats[$fantasy_point_stats[1]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[1]" value="<?php echo $fantasy_point_factors[1];?>">) + 
                        (<?php echo $avail_stats[$fantasy_point_stats[2]]['name'];?> / <input type="text" class="span1" name="fant_pt_factor[2]" value="<?php echo $fantasy_point_factors[2];?>">) + 
                        (<?php echo $avail_stats[$fantasy_point_stats[3]]['name'];?> / <input type="text" class="span1" name="fant_pt_factor[3]" value="<?php echo $fantasy_point_factors[3];?>">)) - 
                        (<?php echo $avail_stats[$fantasy_point_stats[4]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[4]" value="<?php echo $fantasy_point_factors[4];?>">)
                    <?php elseif($player_info->position == 4) :?>

                    (<?php echo $avail_stats[$fantasy_point_stats[0]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[0]" value="<?php echo $fantasy_point_factors[0];?>">) +
                    (<?php echo $avail_stats[$fantasy_point_stats[1]]['name'];?> / <input type="text" class="span1" name="fant_pt_factor[1]" value="<?php echo $fantasy_point_factors[1];?>">) +
                    (<?php echo $avail_stats[$fantasy_point_stats[2]]['name'];?> / <input type="text" class="span1" name="fant_pt_factor[2]" value="<?php echo $fantasy_point_factors[2];?>">) 
                <?php elseif($player_info->position == 8 || $player_info->position == 1 || $player_info->position == 2) :?>
                <?php echo $avail_stats[$fantasy_point_stats[0]]['name'];?> + 
                (<?php echo $avail_stats[$fantasy_point_stats[1]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[1]" value="<?php echo $fantasy_point_factors[1];?>">) +
                (<?php echo $avail_stats[$fantasy_point_stats[2]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[2]" value="<?php echo $fantasy_point_factors[2];?>">) +
                (<?php echo $avail_stats[$fantasy_point_stats[3]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[3]" value="<?php echo $fantasy_point_factors[3];?>">) +
                (<?php echo $avail_stats[$fantasy_point_stats[4]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[4]" value="<?php echo $fantasy_point_factors[4];?>">) +
                (<?php echo $avail_stats[$fantasy_point_stats[5]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[5]" value="<?php echo $fantasy_point_factors[5];?>">)
                <?php elseif($player_info->position == 6 || $player_info->position == 5) :?>
                (<?php echo $avail_stats[$fantasy_point_stats[0]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[0]" value="<?php echo $fantasy_point_factors[0];?>">) +
                (<?php echo $avail_stats[$fantasy_point_stats[1]]['name'];?> / <input type="text" class="span1" name="fant_pt_factor[1]" value="<?php echo $fantasy_point_factors[1];?>">) +
                (<?php echo $avail_stats[$fantasy_point_stats[2]]['name'];?> / <input type="text" class="span1" name="fant_pt_factor[2]" value="<?php echo $fantasy_point_factors[2];?>">)
            <?php elseif($player_info->position == 7) :?>
                (<?php echo $avail_stats[$fantasy_point_stats[0]]['name'];?> + 
                (<?php echo $avail_stats[$fantasy_point_stats[1]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[1]" value="<?php echo $fantasy_point_factors[1];?>">) +
                 <?php echo $avail_stats[$fantasy_point_stats[2]]['name'];?> +
                (<?php echo $avail_stats[$fantasy_point_stats[3]]['name'];?> * <input type="text" class="span1" name="fant_pt_factor[3]" value="<?php echo $fantasy_point_factors[3];?>">) +
                 <?php echo $avail_stats[$fantasy_point_stats[4]]['name'];?> - <?php echo $avail_stats[$fantasy_point_stats[5]]['name'];?>;
            <?php endif;?>
        </td>
    </tr>
    <tr><td>
        <input type="submit" value="Update" class="btn span4" name="update_fant_pts" />
    </td></tr>
</table>
</div>
</div>
</div>
</div>


</div>


</form>
