    <div class="row-fluid">
      <div class="span12">

        <?php echo validation_errors(); ?>

        
        <div class="box">
          <div class="box-title">
            Edit Info for <?php echo $player_info->first_name.' '.$player_info->last_name;?>
          </div>
          <div class="box-content nopadding">
            <form class="form-horizontal" method="post" action="<?php echo current_url();?>">
              <div class="control-group">
                <label class="control-label" for="first_name">First Name</label>
                <div class="controls">
                  <input type="text" id="first_name" placeholder="First Name" name="first_name" value="<?php echo set_value('first_name', $player_info->first_name);?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="last_name">Last Name</label>
                <div class="controls">
                  <input type="text" id="last_name" placeholder="Last Name" name="last_name" value="<?php echo set_value('last_name', $player_info->last_name);?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="rank">Rank</label>
                <div class="controls">
                  <input type="text" id="rank" placeholder="Rank" name="rank" value="<?php echo set_value('rank', $player_info->rank);?>">
                </div>
              </div>



              <div class="control-group">
            <label class="control-label" for="position">Position</label>
            <div class="controls">
              <select id="position" name="position">
                <option value="">--</option>
                <?php foreach ($positions as $position):?>
                <?php $selected = ($position->id == $player_info->position) ? 'selected' : '';?>

                <option value="<?php echo $position->id;?>" <?php echo $selected;?>><?php echo $position->name;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>


              <div class="control-group">
                <label class="control-label" for="age">Age</label>
                <div class="controls">
                  <input type="text" id="age" placeholder="Age" name="age" value="<?php echo set_value('age', $player_info->age);?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="bye_week">Bye Week</label>
                <div class="controls">
                  <input type="text" id="bye_week" placeholder="Bye Week" name="bye_week" value="<?php echo set_value('bye_week', $player_info->bye_week);?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="profile">Profile</label>
                <div class="controls">
                  <textarea class="span10" rows="10" id="profile" placeholder="Profile" name="profile"><?php echo set_value('profile', $player_info->profile);?></textarea>
                </div>
              </div>
   



<div class="control-group">
  <div class="controls">
    <input type="hidden" name="id" value="<?php echo $player_info->id;?>" />
    <button type="submit" class="btn">Submit</button>
  </div>
</div>
</form>

</div>
</div>
</div>
</div>