<?php
require APPPATH .'libraries/REST_Controller.php';

class Api extends REST_Controller {


	function __construct() {
		parent::__construct();
		//$this->cache->delete_all();
		$this->load->model( 'sport_model' );
		$this->load->model( 'team_model' );
		$this->load->model( 'player_model' );
		$this->load->model( 'player_year_model' );
		$this->load->model( 'stat_model' );
		$this->load->model( 'player_multiplier_model' );
		$this->load->model( 'position_model' );
		$this->load->model( 'stat_grade_model' );
		$this->load->model( 'player_year_stat_model' );
		$this->load->model( 'team_year_stat_model' );
		$this->load->model( 'team_year_model' );
	}

	function sports_get() {
		$sports = $this->sport_model->get_all();
		if ( $sports ) {
			$to_output = array();
			$to_output['sports']['result_count'] = count( $sports );
			$to_output['sports']['items'] = $sports;
			$this->response( $to_output, 200 );
		} else {
			$this->response( NULL, 404 );
		}
	}

	function teams_get() {

		if ( $this->get( 'league' ) ) {
			$league = strtoupper( $this->get( 'league' ) );
			$teams = $this->team_model->get_many_by( 'league', $league );
		} else if ( $this->get( 'sport_id' ) ) {
				$teams = $this->team_model->get_many_by( 'sport_id', $this->get( 'sport_id' ) );
			} else {
			//$teams = $this->team_model->get_all();
			//var_dump($teams);
			$teams = $this->cache->model( 'team_model', 'get_all', array(), 120 );
			//var_dump($teams);
		}
		//die();
		if ( $teams ) {
			foreach ( $teams as &$team ) {
				unset( $team->slug );
				unset( $team->created_at );
				//unset($team->updated_at);
			}
			$to_output = array();
			$to_output['teams']['result_count'] = count( $teams );
			$to_output['teams']['items'] = $teams;
			$this->response( $to_output, 200 );
		} else {
			$this->response( NULL, 404 );
		}
	}

	function team_get() {
		if ( !$this->get( 'id' ) ) {
			$this->response( NULL, 400 );
		}
		$to_find = array();
		$to_find['id'] = $this->get( 'id' );
		$team = $this->team_model->get_by( $to_find );

		//if year is set retrieve roster for the year
		if ( $this->get( 'year' ) ) {
			$to_find['year'] = $this->get( 'year' );

			$team_players = $this->player_year_model->get_players_by_team_id_and_year( $this->get( 'id' ), $this->get( 'year' ) );
			$player = array();
			foreach ( $team_players as &$player ) {
				$player_position = $this->position_model->get( $player->position );
				$player->position = $player_position->name;
				//unset($player->team_name);
				//unset($player->team_id);
				//$this->unset_time($player);
			}
			$player_array = array();
			$player_array['result_count'] = count( $team_players );
			$player_array['items'] = $team_players;
			$team->players = $player_array;
		}
		if ( $team ) {
			$to_output = array();
			$to_output['team']['result_count'] = count( $team );
			$to_output['team']['items'] = $team;
			$this->response( $to_output, 200 );
		} else {
			$this->response( NULL, 404 );
		}
	}


	function player_get() {
		//if no id return 404
		if ( !$this->get( 'id' ) ) {
			$this->response( NULL, 400 );
		}

		//get player
		$player = $this->player_model->get( $this->get( 'id' ) );

		//if not player return 404
		if ( !$player ) {
			$this->response( NULL, 400 );
		}
		$this->unset_time( $player );

		//get available stats for players position
		//and reformat array using stat_id as key
		$stats_obj = $this->stat_model->as_array()->get_many_by( 'position_id', $player->position );
		foreach ( $stats_obj as $so ) {
			$stats_array[$so['id']] = $so;
		}

		//change player position from id to name
		$player_position = $this->position_model->get( $player->position );
		$player->position = $player_position->name;

		//if year is set give stats for year
		//else return projections
		$stats = array();
		if ( $this->get( 'year' ) ) {

			$to_find = array();
			$to_find['player_id'] = $this->get( 'id' );
			$to_find['year'] = $this->get( 'year' );
			$player_yearly_info = $this->player_year_model->get_by( $to_find );
			if ( $player_yearly_info ) {
				//get players team for that year and add to player object
				$player->team_id = $player_yearly_info->team_id;
				$team_info = $this->team_model->get( $player->team_id );
				$player->team = $team_info->short_name;
				$player_year_id = $player_yearly_info->id;

				//get players stat by player_yearly_info->id
				$player_stats = $this->player_year_stat_model->get_many_by( 'player_year_id', $player_year_id );
				foreach ( $player_stats as $stat ) {
					//var_dump($stat->stat_id);
					$stats[] = array( 'id' => $stat->stat_id,
						'name' => $stats_array[$stat->stat_id]['short_name'],
						'value' => $stat->value );
					//$year[$stat->stat_id] = $stat->value;
				}
			}

			//add stats to player object
			$stats_array = array();
			$stats_array['result_count'] = count( $stats );
			$stats_array['items'] = $stats;
			$player->stats = $stats_array;
		} else {
			$stats_array['result_count'] = 0;
			$stats_array['items'] = array();
		} //end of if ($this->get('year'))



		if ( $player ) {
			//$player = array_merge($player, );
			$to_output = array();
			$to_output['player']['result_count'] = count( $player );
			$to_output['player']['items'] = $player;
			$this->response( $to_output, 200 );
		} else {
			$this->response( NULL, 404 );
		}


	}

	function get_teams() {
		$years_array = array( 2012, 2011, 2010 );
		//$teams = $this->team_model->get_many_by('league', 'NFL');
		$teams = $this->cache->model( 'team_model', 'get_many_by', array( 'league', 'NFL' ), 7200 );
		//var_dump($teams);die();

		$new_teams = array();
		$player_position = 9;
		//$stats_obj = $this->stat_model->as_array()->get_many_by('position_id', 9);
		//$stats_obj = $this->stat_model->get_many_by_position_id_as_array(9);
		$stats_obj = $this->cache->model( 'stat_model', 'get_many_by_position_id_as_array', array( 9 ), 7200 );
		//var_dump($stats_obj);die();
		foreach ( $stats_obj as $so ) {
			$stats_array_2[$so['id']] = $so;
		}
		//var_dump($stats_array_2[124]);
		foreach ( $teams as $team ) {
			//echo '<hr>';
			//var_dump($team);
			$projections = array();
			$team_output = array();
			$team_output['id'] = 'team_'.$team->id;
			$team_output['first_name'] = $team->city;
			$team_output['last_name'] = $team->name;
			$team_output['position'] = 9;
			$team_output['profile'] = $team->profile;
			$team_output['bye_week'] = $team->bye_week;
			$team_output['auction_value'] = 0;
			$team_output['position_name'] = 'D/ST';


			$report_card = array();
			$stat_branch = array();

			$curr_stats = array();
			foreach ( $stats_array_2 as $avail_stat ) {
				//var_dump($avail_stat);
				//$stats = array();
				$stat_id = $avail_stat['id'];
				$stats['year'] = 2013;
				$stats['pos'] = 9;
				$projection = $this->team_projections( $team->id, $stat_id, 10 );
				$curr_stats[] = array( 'id' => $stat_id,
					'name' => $stats_array_2[$stat_id]['short_name'],
					'value' => $projection );
				//$grade_lookup = $this->stat_grade_model->get_grade($stat_id, $projection);
				$grade_lookup = $this->cache->model( 'stat_grade_model', 'get_grade', array( $stat_id, $projection ), 7200 );
				if ( $grade_lookup ) {
					$report_card[] = array( 'id' => $stat_id,
						'name' => $stats_array_2[$stat_id]['short_name'],
						'grade' =>$grade_lookup->grade );
				}
			}
			$team_output['report_card'] = $report_card;
			//var_dump($report_card);
			$stats_array = array();
			$stats_array['result_count'] = count( $curr_stats );
			$stats_array['items'] = $curr_stats;
			$stats['stats'] = $stats_array;
			//var_dump($curr_stats);

			$stat_branch[] = $stats;

			foreach ( $years_array as $year ) {
				$to_find['team_id'] = $team->id;
				$to_find['year'] = $year;
				//$year_info = $this->team_year_model->get_team_years_by_year_and_team_id($year, $team->id);
				$year_info = $this->cache->model( 'team_year_model', 'get_team_years_by_year_and_team_id', array( $year, $team->id ), 7200 );
				//$year_info = $this->cache->model('team_year_model', 'get_by', $to_find, 7200);
				if ( $year_info ) {
					$team_year_id = $year_info->id;
					//$team_stats = $this->team_year_stat_model->as_array()->get_many_by('team_year_id', $team_year_id);
					$team_stats = $this->cache->model( 'team_year_stat_model', 'get_by_team_year_id_as_array', array( $team_year_id ), 7200 );

					$stats = array();
					$curr_stats = array();
					$stats['year'] = $year_info->year;
					$stats['pos'] = 9;
					foreach ( $team_stats as $team_stat ) {
						//var_dump($team_stat);
						$stat_id = (int) $team_stat['stat_id'];
						if ( $stats_array_2[$stat_id] ) {
							$curr_stats[] = array( 'id' => $stat_id,
								'name' => $stats_array_2[$stat_id]['short_name'],
								'value' => $team_stat['value'] );
						}
					}
					$stats_array = array();
					$stats_array['result_count'] = count( $curr_stats );
					$stats_array['items'] = $curr_stats;
					$stats['stats'] = $stats_array;
					//var_dump($curr_stats);

					$stat_branch[] = $stats;
				}
			}
			$team_output['years'] = $stat_branch;
			$new_teams[] = $team_output;
		}
		return $new_teams;
	}

	function allplayers_get() {

		$to_output = array();
		$current_year = 2013;
		$years_array = array( 2013, 2012, 2011, 2010, 2009, 2008 );
		$stat_filter = array();
		$stat_filter['yearly_stat'] = 1;
		//$stats_obj = $this->stat_model->order_by('sort')->as_array()->get_many_by('yearly_stat', 1);
		//$stats_obj = $this->stat_model->get_yearly_stats(1);
		$stats_obj = $this->cache->model( 'stat_model', 'get_yearly_stats', array( 1 ), 7200 );
		//var_dump($stats_obj);die();

		//$teams = $this->team_model->get_many_by('sport_id', 1);
		$teams = $this->cache->model( 'team_model', 'get_all_teams_by_sport_id', array( 1 ), 7200 );
		//var_dump($teams);die();
		//$positions = $this->position_model->get_all();
		$positions = $this->cache->model( 'position_model', 'get_all', array(), 7200 );

		$position_array = array();
		foreach ( $positions as $pos ) {
			$position_array[$pos->id] = $pos->name;
		}
		foreach ( $teams as $team ) {
			$team_array[$team->id] = $team->short_name;
		}
		//var_dump($team_array);die();
		foreach ( $stats_obj as $so ) {
			//var_dump($so);die();
			$stats_obj_array[$so['position_id']][$so['id']] = $so;
		}


		//$player_years = $this->player_year_model->get_player_years_array($current_year);
		$player_years = $this->cache->model( 'player_year_model', 'get_player_years_array', array( $current_year ), 7200 );

		$players = array();
		foreach ( $player_years as $py ) {


			//$player_object = $this->player_model->get($py['player_id']);
			//var_dump($player_object);die();
			$player_object = $this->cache->model( 'player_model', 'get', array( $py['player_id'] ), 7200 );

			unset( $player_object->full_name );
			//var_dump($player_object->position);die();
			$player_object->position_name = $position_array[$player_object->position];


			$report_card = array();
			$this->unset_time( $player_object );
			$stat_branch = array();
			foreach ( $years_array as $year ) {
				//get info for player year
				$to_find['player_id'] = $player_object->id;
				$to_find['year'] = $year;
				//$year_info = $this->player_year_model->get_by($to_find);
				$year_info = $this->cache->model( 'player_year_model', 'get_by', array( $to_find ), 7200 );
				//var_dump($year_info);die();
				if ( $year_info ) {
					$stats = array();
					$curr_stats = array();
					$stats['year'] = $year_info->year;
					$stats['team_id'] = $year_info->team_id;
					$stats['team'] = ( $year_info->team_id == 0 ) ? '' : $team_array[$year_info->team_id];
					$stats['pos'] = $year_info->pos;

					$player_year_id = $year_info->id;
					//$player_stats = $this->player_year_stat_model->as_array()->get_many_by('player_year_id', $player_year_id);

					//$player_stats = $this->player_year_stat_model->get_by_player_year_id_as_array($player_year_id);
					$player_stats = $this->cache->model( 'player_year_stat_model', 'get_by_player_year_id_as_array', array( $player_year_id ), 7200 );
					//var_dump($player_stats);die();
					//var_dump($player_year_id);die();
					foreach ( $player_stats as $player_stat ) {
						$player_position = $player_object->position;
						$stat_id = $player_stat['stat_id'];
						if ( isset( $stats_obj_array[$player_position][$stat_id] ) ) {
							$curr_stats[] = array( 'id' => $player_stat['stat_id'],
								'name' => $stats_obj_array[$player_position][$stat_id]['short_name'],
								'value' => $player_stat['value'] );
						}

					}
					if ( $year == 2013 ) {
						$report_card = array();
						foreach ( $curr_stats as $cs ) {
							//$grade_lookup = $this->stat_grade_model->get_grade($cs['id'], $cs['value']);
							$grade_lookup = $this->cache->model( 'stat_grade_model', 'get_grade', array( $cs['id'], $cs['value'] ), 7200 );
							if ( $grade_lookup ) {
								$report_card[] = array( 'id' => $cs['id'],
									'name' => $cs['name'],
									'grade' =>$grade_lookup->grade );
							}
						}
					}
					foreach ($curr_stats as &$cs)
					{
						unset($cs['id']);
					}
					$stats_array = array();
					//$stats_array['result_count'] = count( $curr_stats );
					$stats_array['items'] = $curr_stats;
					$stats['stats'] = $stats_array;
					$stat_branch[] = $stats;
				}
			}
			$player_object->report_card = $report_card;
			$player_object->years = $stat_branch;
			$players[] = $player_object;
		} //end of foreach loop
		//get all players from 2013

		$teams = $this->get_teams();
		$players = array_merge( $players, $teams );


		//$to_output['players']['result_count'] = count( $players );
		$to_output['players']['items'] = $players;

		$this->response( $to_output, 200 );
	}

	function players_get() {
		$stats_obj = $this->stat_model->as_array()->get_all();
		foreach ( $stats_obj as $so ) {
			//var_dump($so);die();
			$stats_obj_array[$so['position_id']][$so['id']] = $so;
		}
		$player_array = array();
		if ( !$this->get( 'year' ) ) {
			$this->response( NULL, 404 );
		}
		$player_years = $this->player_year_model->as_array()->get_many_by( 'year', $this->get( 'year' ) );
		$players = array();
		foreach ( $player_years as $py ) {
			$stats = array();

			$player_object = $this->player_model->as_array()->get( $py['player_id'] );

			$player_object['team_id'] = $py['team_id'];
			$team_info = $this->team_model->as_array()->get( $player_object['team_id'] );

			$player_object['team'] = $team_info['short_name'];
			$player_year_id = $py['id'];
			$player_stats = $this->player_year_stat_model->as_array()->get_many_by( 'player_year_id', $player_year_id );
			foreach ( $player_stats as $stat ) {
				$player_position = $player_object['position'];
				$stat_id = $stat['stat_id'];
				$stats[] = array( 'id' => $stat['stat_id'],
					'name' => $stats_obj_array[$player_position][$stat_id]['short_name'],
					'value' => $stat['value'] );
			}
			$stats_array = array();
			$stats_array['result_count'] = count( $stats );
			$stats_array['items'] = $stats;
			$player_object['stats'] = $stats_array;

			$players[] = $player_object;
		}
		$to_output['players']['result_count'] = count( $players );
		$to_output['players']['items'] = $players;
		$this->response( $to_output, 200 );
	}

	private function team_projections( $team_id, $stat_id, $mult_value = 10 ) {
		$base_year = 2012;
		$end_year = $base_year - 3;
		$multipliers_count = 0;
		$multipliers = array( 5, 3, 1.5, .5 );
		//$team_info = $this->team_model->get($team_id);
		$team_info = $this->cache->model( 'team_model', 'get', array( $team_id ), 7200 );
		//$team_stat = $this->stat_model->get_many_by('position_id', 9);
		$team_stat = $this->cache->model( 'stat_model', 'get_many_by', array( 'position_id', 9 ), 7200 );
		//var_dump($team_stat);die();
		//$stat_info = $this->stat_model->get($stat_id);
		$stat_info = $this->cache->model( 'stat_model', 'get', array( $stat_id ), 7200 );

		//$stats = $this->team_year_model->get_projection($team_id, $stat_id);
		$stats = $this->cache->model( 'team_year_model', 'get_projection', array( $team_id, $stat_id ), 7200 );
		//var_dump($stats);die();
		$stat_work = array();

		foreach ( $stats as $stat ) {
			$stat_work[$stat->year] = $stat->value;
		}

		$projection_result = 0;
		for ( $year = $base_year; $year>=$end_year; $year-- ) {
			if ( isset( $stat_work[$year] ) ) {
				$projection_result += $stat_work[$year] * $multipliers[$multipliers_count];
			}
			$multipliers_count++;

		}
		return round( $projection_result / $mult_value );

	}
	private function unset_time( &$object ) {
		unset( $object->created_at );
		unset( $object->updated_at );
	}


} //end of class
