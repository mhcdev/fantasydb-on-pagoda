<?php  if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Stat_model extends MY_Model
{

  public $_table = 'stats';
  public $before_create = array( 'created_at', 'updated_at', 'add_sort');
  public $before_update = array( 'updated_at' );

  protected function add_sort($row)
  {
      //get max sort from
    $sort_temp = $this->order_by('sort', 'desc')->limit(1)->get_by('position_id', $row['position_id']);
    if ($sort_temp)
    {
      $next_sort = $sort_temp->sort + 1;
    } else {
      $next_sort = 1;
    }
    $row['sort'] = $next_sort;
    return $row;
  }


  function get_many_by_position_id_as_array($position_id)
  {
    $this->db->from($this->_table);
    $this->db->where('position_id', $position_id);
    $rows = $this->db->get()->result_array();
    return $rows;
    //return $row = $this->as_array->get_all();
  }

  function get_yearly_stats($yearly_stat)
  {
    $this->db->from($this->_table);
    $this->db->where('yearly_stat', 1);
    $this->db->order_by('sort');
    $rows = $this->db->get()->result_array();
    return $rows;
  }

} //end of model
