<?php  if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Player_year_stat_model extends MY_Model
{

	public $_table = 'player_years_stats';
	public $before_create = array( 'created_at', 'updated_at');
	public $before_update = array( 'updated_at' );

	function get_by_player_year_id_as_array($player_year_id)
	{
		$this->db->from($this->_table);
		$this->db->where('player_year_id', $player_year_id);
		$rows = $this->db->get()->result_array();
		return $rows;
		//return $row = $this->as_array->get_all();
	}


} //end of model
