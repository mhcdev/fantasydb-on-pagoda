$(document).ready(function() {
	
	 $('#position_players_table').dataTable({
        "sPaginationType": "full_numbers",
         "iDisplayLength": 50,
         "aaSorting": [[ 2, "desc" ]]
    });   

	 $('#players_table').dataTable({
        "sPaginationType": "full_numbers",
         "iDisplayLength": 50,
         "aaSorting": [[ 3, "desc" ]]
    });  


    var eq_option = $('#projection_equation').val();
	show_alt_eq(eq_option);
	
	$('#projection_equation').change(function() {
		var selected_val = $(this).val()
		show_alt_eq(selected_val)
	});


	function show_alt_eq(selected_val)
	{
		if (selected_val == 0)
		{
			$('#alt_eq_container').show();
		} else {
			$('#alt_eq_container').hide();
		}
	} 


});