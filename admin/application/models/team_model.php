<?php  if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Team_model extends MY_Model
{

	public $_table = 'teams';
	public $before_create = array( 'created_at', 'updated_at');
	public $before_update = array( 'updated_at' );



	function get_team_ci($team_name)
	{
		$this->db->where('LOWER(name)', strtolower($team_name));
		return $this;
	}

} //end of model
