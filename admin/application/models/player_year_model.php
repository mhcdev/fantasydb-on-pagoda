<?php  if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Player_year_model extends MY_Model
{

	public $_table = 'player_years';
	public $before_create = array( 'created_at', 'updated_at');
	public $before_update = array( 'updated_at' );


	function available_team_years($team_id)
	{ 
		$this->db->distinct();
		$this->db->select('year');
		$this->db->order_by('year');
		return $row = $this->get_all($team_id); 
	}

	function get_projection($player_id, $stat_id, $modifier = null)
	{
		$this->db->join('player_years_stats', 'player_years.id = player_years_stats.player_year_id');
		$this->db->where('player_years.player_id', $player_id);	
		$this->db->where('player_years_stats.stat_id', $stat_id);
		return $row = $this->get_all();
	}

	function get_players_from_year($year)
	{
		$this->db->select('players.id, players.first_name, players.last_name, players.position');
		$this->db->select('player_years.pos');
		$this->db->select('teams.id as team_id, teams.short_name as team_name');
		$this->db->join('players', 'players.id = player_years.player_id');
		$this->db->join('teams', 'teams.id = player_years.team_id');
		$this->db->where('player_years.year', $year);	
		return $row = $this->get_all();
	}

	function get_players_by_team_id_and_year($team_id, $year)
	{
		$this->db->select('players.id, players.first_name, players.last_name, players.position');
		$this->db->select('player_years.pos');
		$this->db->select('teams.id as team_id, teams.short_name as team_name');
		$this->db->join('players', 'players.id = player_years.player_id');
		$this->db->join('teams', 'teams.id = player_years.team_id');
		$this->db->where('teams.id', $team_id);	
		$this->db->where('player_years.year', $year);	
		return $row = $this->get_all();
	}


	function get_player_year_info($player_id, $year)
	{
		$this->db->select('teams.id as team_id, teams.short_name as team_name');
		$this->db->join('teams', 'teams.id = player_years.team_id');
		//$this->db->where('player_years.year', $year);	
		$this->db->where('player_years.player_id', $player_id);	
		return $row = $this->get_by('year', $year);
	}
	
} //end of model
