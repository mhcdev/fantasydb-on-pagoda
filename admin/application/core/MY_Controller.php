<?php

class MY_Controller extends CI_Controller
{
    var $user = FALSE;

    // layout / autoview functionality
    protected $layout_view = 'application';
    protected $content_view = '';
    protected $view_data = array();
    protected $active_menu = 'dashboard';
    protected $page_data = array();
    protected $the_user;

    function __construct()
    {
        parent::__construct();

/*
        if ( $this->ion_auth->logged_in() ) {

          //Put User in Class-wide variable
          $this->the_user = $this->ion_auth->user()->row();
     
          //Store user in $data
          $data = new StdClass;
          $data->the_user = $this->the_user;

          //Load $the_user in all views
          $this->load->vars($data);
        }
        */
       // else {
        //  redirect('login');
       // }
       //$this->output->enable_profiler(TRUE);

      $this->form_validation->set_error_delimiters('<div class="row-fluid"><div class="alert alert-error">', '</div></div>');
    }


    public function _output($output)
    {
      $data = new StdClass;
      $data->page_data = $this->page_data;
      //var_dump($data);die();
      $this->load->vars($data);
      // set the default content view
        if($this->content_view !== FALSE && empty($this->content_view)) $this->content_view = $this->router->class . '/' . $this->router->method;

      if ($this->router->directory == 'admin/')
      {
        $this->content_view = 'admin/'.$this->content_view;
      }
        // render the content view
        $yield = file_exists(APPPATH . 'views/' . $this->content_view . '.php') ? $this->load->view($this->content_view, $this->view_data, TRUE) : FALSE;
        // render the layout
        if($this->layout_view)
            echo $this->load->view('layouts/' . $this->layout_view, array('yield' => $yield, 'active_menu' => $this->active_menu), TRUE);
        else
            echo $yield;

        //output profiler information
        echo $output;
    }



}
