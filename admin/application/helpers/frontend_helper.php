<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function FR_lookup_id($model, $id, $field = 'name')
	{
		 $CI =& get_instance();
		$CI->load->model($model, 'model_to_use');
		$result = $CI->model_to_use->get($id);
		return $result->$field;
	}