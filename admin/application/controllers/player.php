<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('team_model');
		$this->load->model('player_model');
		$this->load->model('player_year_model');
		$this->load->model('position_model');
		$this->load->model('stat_model');
		$this->load->model('stat_grade_model');
		$this->load->model('player_year_stat_model');
		$this->load->model('player_multiplier_model');
		$this->load->model('player_fpfactor_model');
	}

	function index()
	{
		$this->view_data['players'] = $this->player_model->get_all();
	}

	function current_players()
	{
		//die();
		$this->load->library('curl');
		$years_array = array(2012, 2011, 2010, 2009, 2008);
		$current_year = 2013;
		$player_years = $this->player_year_model->as_array()->get_many_by('year', $current_year);
		foreach ($player_years as $py)
		{
			$positions[$py['pos']][] = $py;
			$this->view_data['players'][] = $this->player_model->get($py['player_id']);
		}
		//var_dump(count($positions['TE']));
		//var_dump($positions);
		//var_dump($this->view_data['players']);die();
		
		foreach ($this->view_data['players'] as $i => $player)
		{
			$link = 'http://fantasydb.mhcdev.net/player/view/'.$player->id.'/'.$i;
			echo $i.' '.$link.'<BR>';
			$x =  $this->curl->simple_get($link);
			var_dump($x);
			echo '<hr/>';
			//if ($i>20) die();
			//die();
			//die();
		}
		
		die();
	}

	function edit($id)
	{
		if ($this->input->post())
		{
			//var_dump($this->input->post());die();
			$this->form_validation->set_rules('first_name', "First Name", 'trim|xss_clean|required');
			$this->form_validation->set_rules('last_name', "Last Name", 'trim|xss_clean|required');
			$this->form_validation->set_rules('position', "Position", 'trim|xss_clean|required');
			$this->form_validation->set_rules('age', "Age", 'trim|xss_clean|required');
			$this->form_validation->set_rules('rank', "Rank", 'trim|xss_clean|required');
			$this->form_validation->set_rules('bye_week', "Bye Week", 'trim|xss_clean|required');
			$this->form_validation->set_rules('profile', "profile", 'trim|xss_clean|required');
			if ($this->form_validation->run() == true)
			{
				$to_save = array();
				$to_save['first_name'] = $this->input->post('first_name');
				$to_save['last_name'] = $this->input->post('last_name');
				$to_save['position'] = $this->input->post('position');
				$to_save['age'] = $this->input->post('age');
				$to_save['rank'] = $this->input->post('rank');
				$to_save['bye_week'] = $this->input->post('bye_week');
				$to_save['profile'] = $this->input->post('profile');
				$new = $this->player_model->update($id, $to_save);
				if ($new)
				{
					redirect('player/view/'.$id);
				}
			}
		}
		$this->view_data['player_info'] = $this->player_model->get($id);
		$this->view_data['positions'] = $this->position_model->get_all();
	}
/*
	private function fp_work()
	{
		$players = $this->player_model->get_all();
		$count = 0;
		foreach ($players as $player_info)
		{
			//$player_info = $this->player_model->get(1593);
			if (is_null($player_info->fp))
			{

			$id = $player_info->id;
			//var_dump($player);
			$player_yearly_info = $this->player_year_model->get_many_by('player_id', $id);
			foreach ($player_yearly_info as &$year)
			{
				$year->team_info = $this->team_model->get($year->team_id);
				$player_stats = $this->player_year_stat_model->get_many_by('player_year_id', $year->id);
				foreach ($player_stats as $stat)
				{
					$year->stats[$stat->stat_id] = $stat->value;
				}
			}
			$avail_stats = $this->stat_model->get_many_by('position_id', $player_info->position);
		//get player_multi
			$player_multis = $this->player_multiplier_model->get_many_by('player_id', $id);
			$stat_mulitpliers = array();
			foreach ($player_multis as $multis)
			{
				$stat_mulitpliers[$multis->stat_id] = $multis->multiplier;
			}
			$stat_multiplier = 10;



			foreach ($avail_stats as $stat)
		{

			if (isset($stat_mulitpliers[$stat->id]))
			{
				$stat_multiplier = $stat_mulitpliers[$stat->id];
			} else {
				$stat_multiplier = 10;
			}
			$this->view_data['avail_stats'][$stat->id]  = array( 'projection' => $stat->projection,
				'name' => $stat->short_name,
				'multi' => $stat_multiplier);
			if ($stat->projection == 'projection_formula')
			{
				//var_dump(is_null($stat->projection));
				//var_dump($stat->projection);
				$to_find = array();
				$to_save['player_id'] = $id;
				$to_save['stat_id'] = $stat->id;
				$projections[$stat->id] = $this->projections($id, $stat->id,$stat_multiplier);
			} else {
				if (!$stat->alt_eq)
				{
					$to_find = array();
					$to_save['player_id'] = $id;
					$to_save['stat_id'] = $stat->id;
					//$this->view_data['projections'][$stat->id] = $this->projections($id, $stat->id,$stat_multiplier);
					$projections[$stat->id] = 0;
				} else {
					$stat_work = unserialize($stat->alt_eq);
					//var_dump($stat_work);
					$output_eq = '(';
						foreach ($stat_work as $i => $value)
						{
						//var_dump($i);
							if (strlen($value)>0 )
							{
							//var_dump(preg_match('/^[0-9]{1,}$/', $value));
							//if (is_numeric($value) && $i != 'factor')
								if (!is_int($i))
								{
									$output_eq .= ''.$value.' ';
								} elseif (in_array($value, array('+', '-', '/', '*'))) {
									$output_eq .= ' '.$value.' ';
								} else {
									if (isset($projections[$value]))
									{
										$output_eq .= $projections[$value].' ';
									}
								}
								//var_dump($stat->id.' '.$output_eq);
							}
							if ($i == 2) $output_eq .= ')';
}

					//var_dump($output_eq);
$projections[$stat->id]  = round($this->calculate_string($output_eq),1);
}
}

} //end of avail_stats foreach


$fp = 0;
if ($player_info->position == 3)
{
	$projections[41] = $this->qb_fts($id, $projections);
	$fp = $projections[41];
} elseif ($player_info->position == 4)
{
	$projections[60] = $this->rb_fts($id, $projections);
	$fp = $projections[60];
} elseif ($player_info->position == 8) {
	$projections[117] = $this->lb_fts($id, $projections);
	$fp = $projections[117];
} elseif ($player_info->position == 1) {
	$projections[12] = $this->db_fts($id, $projections);
	$fp = $projections[12];
} elseif ($player_info->position == 2) {
	$projections[24] = $this->dl_fts($id, $projections);
	$fp = $projections[24];
} elseif ($player_info->position == 6) {
	$projections[88] = $this->te_fts($id, $projections);
	$fp = $projections[88];
} elseif ($player_info->position == 5) {
	$projections[74] = $this->wr_fts($id, $projections);
	$fp = $projections[74];
} elseif ($player_info->position == 7) {
	$projections[105] = $this->kicker_fts($id, $projections);
	$fp = $projections[105];
}

	$to_save = array();
	$to_save['fp'] = $fp;
	$this->player_model->update($player_info->id, $to_save);
	$count++;
		if ($count >= 500) die();
		echo $count;
	var_dump($fp);
		var_dump($projections);
			var_dump($player_info);
			 flush();

		} //end of players foreach
		
}			

	} //END OF FP_WORK
*/


	function view($id)
	{
		if ($this->input->post())
		{
			if ($this->input->post('update_fant_pts'))
			{
				$fan_pt_factors = serialize($this->input->post('fant_pt_factor'));
				//var_dump($fan_pt_factors);die();
				$to_save = array();
				$to_save['player_id'] = $id;
				$to_save['multipliers'] = $fan_pt_factors;
				$factor_check = $this->player_fpfactor_model->get_by('player_id', $id);
				if ($factor_check)
				{
					$update = $this->player_fpfactor_model->update($factor_check->id, $to_save);
				} else {
					$new = $this->player_fpfactor_model->insert($to_save);
				}
			} else {
				$this->update_multipliers();
			}	
			
		}
		$this->view_data['player_info'] = $this->player_model->get($id);
		if ($this->view_data['player_info']->position == 3)
		{
			$stats_array[0] = 32;
			$stats_array[1] = 38;
			$stats_array[2] = 29;
			$stats_array[3] = 35;
			$stats_array[4] = 33;
			$this->view_data['fantasy_point_stats'] = $stats_array;

			$factors = $this->player_fpfactor_model->get_by('player_id', $id);
			if (!$factors)
			{
				$factor_array[0] = 3;
				$factor_array[1] = 6;
				$factor_array[2] = 50;
				$factor_array[3] = 25;
				$factor_array[4] = null;
				$this->view_data['fantasy_point_factors'] = $factor_array;
			} else {
				$this->view_data['fantasy_point_factors'] = unserialize($factors->multipliers);
			}
		} elseif($this->view_data['player_info']->position == 4) {
			$stats_array[0] = 57;
			$stats_array[1] = 56;
			$stats_array[2] = 48;
			$this->view_data['fantasy_point_stats'] = $stats_array;

			$factors = $this->player_fpfactor_model->get_by('player_id', $id);
			if (!$factors)
			{
				$factor_array[0] = 6;
				$factor_array[1] = 25;
				$factor_array[2] = 2;
				$this->view_data['fantasy_point_factors'] = $factor_array;
			} else {
				$this->view_data['fantasy_point_factors'] = unserialize($factors->multipliers);
			}
		} elseif($this->view_data['player_info']->position == 8) {
			$stats_array[0] = 107;
			$stats_array[1] = 110;
			$stats_array[2] = 112;
			$stats_array[3] = 114;
			$stats_array[4] = 115;
			$stats_array[5] = 116;
			$this->view_data['fantasy_point_stats'] = $stats_array;

			$factors = $this->player_fpfactor_model->get_by('player_id', $id);
			if (!$factors)
			{
				$factor_array[0] = null;
				$factor_array[1] = 2;
				$factor_array[2] = 3;
				$factor_array[3] = 4;
				$factor_array[4] = 5;
				$factor_array[5] = 6;
				$this->view_data['fantasy_point_factors'] = $factor_array;
			} else {
				$this->view_data['fantasy_point_factors'] = unserialize($factors->multipliers);
			}
		} elseif($this->view_data['player_info']->position == 1) {
			$stats_array[0] = 2;
			$stats_array[1] = 5;
			$stats_array[2] = 7;
			$stats_array[3] = 9;
			$stats_array[4] = 10;
			$stats_array[5] = 11;
			$this->view_data['fantasy_point_stats'] = $stats_array;

			$factors = $this->player_fpfactor_model->get_by('player_id', $id);
			if (!$factors)
			{
				$factor_array[0] = null;
				$factor_array[1] = 2;
				$factor_array[2] = 3;
				$factor_array[3] = 4;
				$factor_array[4] = 5;
				$factor_array[5] = 6;
				$this->view_data['fantasy_point_factors'] = $factor_array;
			} else {
				$this->view_data['fantasy_point_factors'] = unserialize($factors->multipliers);
			}
		} elseif($this->view_data['player_info']->position == 2) {
			$stats_array[0] = 14;
			$stats_array[1] = 17;
			$stats_array[2] = 19;
			$stats_array[3] = 21;
			$stats_array[4] = 22;
			$stats_array[5] = 23;
			$this->view_data['fantasy_point_stats'] = $stats_array;

			$factors = $this->player_fpfactor_model->get_by('player_id', $id);
			if (!$factors)
			{
				$factor_array[0] = null;
				$factor_array[1] = 2;
				$factor_array[2] = 3;
				$factor_array[3] = 4;
				$factor_array[4] = 5;
				$factor_array[5] = 6;
				$this->view_data['fantasy_point_factors'] = $factor_array;
			} else {
				$this->view_data['fantasy_point_factors'] = unserialize($factors->multipliers);
			}
		} elseif($this->view_data['player_info']->position == 6) {
			$stats_array[0] = 85;
			$stats_array[1] = 79;
			$stats_array[2] = 76;
			$this->view_data['fantasy_point_stats'] = $stats_array;

			$factors = $this->player_fpfactor_model->get_by('player_id', $id);
			if (!$factors)
			{
				$factor_array[0] = 6;
				$factor_array[1] = 25;
				$factor_array[2] = 2;
				$this->view_data['fantasy_point_factors'] = $factor_array;
			} else {
				$this->view_data['fantasy_point_factors'] = unserialize($factors->multipliers);
			}
		} elseif($this->view_data['player_info']->position == 5) {
			$stats_array[0] = 71;
			$stats_array[1] = 65;
			$stats_array[2] = 62;
			$this->view_data['fantasy_point_stats'] = $stats_array;

			$factors = $this->player_fpfactor_model->get_by('player_id', $id);
			if (!$factors)
			{
				$factor_array[0] = 6;
				$factor_array[1] = 25;
				$factor_array[2] = 2;
				$this->view_data['fantasy_point_factors'] = $factor_array;
			} else {
				$this->view_data['fantasy_point_factors'] = unserialize($factors->multipliers);
			}
		} elseif($this->view_data['player_info']->position == 7) {
			$stats_array[0] = 102;
			$stats_array[1] = 98;
			$stats_array[2] = 94;
			$stats_array[3] = 96;
			$stats_array[4] = 99;
			$stats_array[5] = 98;
			$this->view_data['fantasy_point_stats'] = $stats_array;

			$factors = $this->player_fpfactor_model->get_by('player_id', $id);
			if (!$factors)
			{
				$factor_array[0] = null;
				$factor_array[1] = 3;
				$factor_array[2] = null;
				$factor_array[3] = 2;
				$factor_array[4] = null;
				$factor_array[5] = null;
				$this->view_data['fantasy_point_factors'] = $factor_array;
			} else {
				$this->view_data['fantasy_point_factors'] = unserialize($factors->multipliers);
			}
		}

		$this->view_data['player_yearly_info'] = $this->player_year_model->get_many_by('player_id', $id);
		foreach ($this->view_data['player_yearly_info'] as &$year)
		{
			$year->team_info = $this->team_model->get($year->team_id);
			$player_stats = $this->player_year_stat_model->get_many_by('player_year_id', $year->id);
			foreach ($player_stats as $stat)
			{
				$year->stats[$stat->stat_id] = $stat->value;
			}
		}
		$avail_stats = $this->stat_model->get_many_by('position_id', $this->view_data['player_info']->position);
		//get player_multi
		$player_multis = $this->player_multiplier_model->get_many_by('player_id', $id);
		foreach ($player_multis as $multis)
		{
			$stat_mulitpliers[$multis->stat_id] = $multis->multiplier;
		}
		$stat_multiplier = 10;
		//var_dump($avail_stats);
		foreach ($avail_stats as $stat)
		{
			
			if (isset($stat_mulitpliers[$stat->id]))
			{
				$stat_multiplier = $stat_mulitpliers[$stat->id];
			} else {
				$stat_multiplier = 10;
			}

			$this->view_data['avail_stats'][$stat->id]  = array( 'projection' => $stat->projection,
				'name' => $stat->short_name,
				'multi' => $stat_multiplier);

			if ($stat->projection == 'projection_formula')
			{
				
				//var_dump(is_null($stat->projection));
				//var_dump($stat->projection);
				$to_find = array();
				$to_save['player_id'] = $id;
				$to_save['stat_id'] = $stat->id;
				$this->view_data['projections'][$stat->id] = $this->projections($id, $stat->id,$stat_multiplier);
			} else {
				if (!$stat->alt_eq)
				{
					//var_dump('a '.$stat->id);
					$to_find = array();
					$to_save['player_id'] = $id;
					$to_save['stat_id'] = $stat->id;
					//$this->view_data['projections'][$stat->id] = $this->projections($id, $stat->id,$stat_multiplier);
					$this->view_data['projections'][$stat->id] = 0;
				} else {
					$stat_work = unserialize($stat->alt_eq);

		


					$output_eq = '(';
						foreach ($stat_work as $i => $value)
						{

							if ($i == 'factor' && strlen($value) == 0)
							{

							} elseif (strlen($value)>0 ) {
							//var_dump(preg_match('/^[0-9]{1,}$/', $value));
							//if (is_numeric($value) && $i != 'factor')
								if (!is_int($i))
								{
									$output_eq .= ''.$value.' ';
								} elseif (in_array($value, array('+', '-', '/', '*'))) {
									$output_eq .= ' '.$value.' ';
								} else {
									if (isset($this->view_data['projections'][$value]))
									{
										$output_eq .= $this->view_data['projections'][$value].' ';
									}
								}
								//var_dump($stat->id.' '.$output_eq);
							}

							if ($i == 2) $output_eq .= ')';
						}

					
					//var_dump($output_eq);
					$this->view_data['projections'][$stat->id]  = round($this->calculate_string($output_eq),1);
				}
			}	
		//var_dump($this->view_data['projections']);

		}
//var_dump($this->view_data['projections']);die();

if ($this->view_data['player_info']->position == 3)
{
	$this->view_data['projections'][41] = $this->qb_fts($id, $this->view_data['projections']);
} elseif ($this->view_data['player_info']->position == 4)
{
	$this->view_data['projections'][60] = $this->rb_fts($id, $this->view_data['projections']);
} elseif ($this->view_data['player_info']->position == 8) {
	$this->view_data['projections'][117] = $this->lb_fts($id, $this->view_data['projections']);
} elseif ($this->view_data['player_info']->position == 1) {
	$this->view_data['projections'][12] = $this->db_fts($id, $this->view_data['projections']);
} elseif ($this->view_data['player_info']->position == 2) {
	$this->view_data['projections'][24] = $this->dl_fts($id, $this->view_data['projections']);
} elseif ($this->view_data['player_info']->position == 6) {
	$this->view_data['projections'][88] = $this->te_fts($id, $this->view_data['projections']);
} elseif ($this->view_data['player_info']->position == 5) {
	$this->view_data['projections'][74] = $this->wr_fts($id, $this->view_data['projections']);
} elseif ($this->view_data['player_info']->position == 7) {
	$this->view_data['projections'][105] = $this->kicker_fts($id, $this->view_data['projections']);
}
$report_card = array();
foreach ($this->view_data['projections'] as $x => $projection)
{
	//check grades
	//$this->stat_grade_model->get_grade($stat_id, $value);
	//var_dump($x);
	//var_dump($projection);
	$grade_lookup = $this->stat_grade_model->get_grade($x, $projection);
	if ($grade_lookup)
	{
		$report_card[$x] = $grade_lookup->grade;
	}
}
var_dump($report_card);
//update players projectsions for current_year;
//var_dump($this->view_data['projections']);
//

//save fp to player_profile
$fp_array = array(3 => 41,
				 4=> 60,
				 8=> 117,
				 1 => 12,
				 2 => 24, 
				 6 => 88,
				 5 => 74,
				 7 => 105);
	$to_save = array();
	$to_save['fp'] = $this->view_data['projections'][$fp_array[$this->view_data['player_info']->position]];
	//var_dump($to_save);die();
	$this->player_model->update($this->view_data['player_info']->id, $to_save);

$to_find = array();
$to_find['year'] = 2013;
$to_find['player_id'] = $this->view_data['player_info']->id;
//var_dump($to_find);die();
$player_year_id = $this->player_year_model->get_by($to_find);
if ($player_year_id)
{
	$player_year_id = $player_year_id->id;	
} else {
	$player_year_id = $this->player_year_model->insert($to_find);
}
//var_dump($player_year_id);die();
foreach ($this->view_data['projections'] as $stat_id => $projection)
{
	//die('a');
	$to_save = array();
	$to_save['player_year_id'] =  $player_year_id;
	$to_save['stat_id'] = $stat_id;
//var_dump($to_save);
	$stat_info = $this->player_year_stat_model->get_by($to_save);

	$to_save['value'] = $projection;

	if ($stat_info)
	{
		$this->player_year_stat_model->update($stat_info->id, $to_save);
	} else {
		$this->player_year_stat_model->insert($to_save);
	}
	//var_dump($to_save);
}
//die();
}

function kicker_fts($player_id, $stat_projections)
{
		//((S_32 * 4) + (S_38 * 6) + (S_29 / 50) + (S_35 / 25)) - (S_33 * 2)
		//fetch players multiplers from player_fpfactor_model
	$factors = $this->player_fpfactor_model->get_by('player_id', $player_id);
	$stats_array[0] = 102;
	$stats_array[1] = 98;
	$stats_array[2] = 94;
	$stats_array[3] = 96;
	$stats_array[4] = 99;
	$stats_array[5] = 98;
	foreach ($stats_array as $key => $stat)
	{
		$projection_stat_array[$key] = $stat_projections[$stat];
	}
	if (!$factors)
	{
		$factor_array[0] = null;
		$factor_array[1] = 3;
		$factor_array[2] = null;
		$factor_array[3] = 2;
		$factor_array[4] = null;
		$factor_array[5] = null;
	} else {
		$factor_array = unserialize($factors->multipliers);
	}
	$eq = ($projection_stat_array[0] + 
		($projection_stat_array[1] * $factor_array[1]) +
		$projection_stat_array[2] +
		($projection_stat_array[3] * $factor_array[3])) -
	$projection_stat_array[5] - $projection_stat_array[5];
	return $eq;
}

function rb_fts($player_id, $stat_projections)
{
		//((S_32 * 4) + (S_38 * 6) + (S_29 / 50) + (S_35 / 25)) - (S_33 * 2)
		//fetch players multiplers from player_fpfactor_model
	$factors = $this->player_fpfactor_model->get_by('player_id', $player_id);
	$stats_array[0] = 57;
	$stats_array[1] = 56;
	$stats_array[2] = 48;
	foreach ($stats_array as $key => $stat)
	{
		$projection_stat_array[$key] = $stat_projections[$stat];
	}
	if (!$factors)
	{
		$factor_array[0] = 6;
		$factor_array[1] = 25;
		$factor_array[2] = 2;
	} else {
		$factor_array = unserialize($factors->multipliers);
	}
	$eq = ($projection_stat_array[0] * $factor_array[0]) + ($projection_stat_array[1] / $factor_array[1]) 
	+ ($projection_stat_array[2] / $factor_array[2]);
	return $eq;
}

function te_fts($player_id, $stat_projections)
{
		//((S_32 * 4) + (S_38 * 6) + (S_29 / 50) + (S_35 / 25)) - (S_33 * 2)
		//fetch players multiplers from player_fpfactor_model
	$factors = $this->player_fpfactor_model->get_by('player_id', $player_id);
	$stats_array[0] = 85;
	$stats_array[1] = 79;
	$stats_array[2] = 76;
	foreach ($stats_array as $key => $stat)
	{
		$projection_stat_array[$key] = $stat_projections[$stat];
	}
	if (!$factors)
	{
		$factor_array[0] = 6;
		$factor_array[1] = 25;
		$factor_array[2] = 2;
	} else {
		$factor_array = unserialize($factors->multipliers);
	}
	$eq = ($projection_stat_array[0] * $factor_array[0]) + ($projection_stat_array[1] / $factor_array[1]) 
	+ ($projection_stat_array[2] / $factor_array[2]);
	return $eq;
}

function wr_fts($player_id, $stat_projections)
{
		//((S_32 * 4) + (S_38 * 6) + (S_29 / 50) + (S_35 / 25)) - (S_33 * 2)
		//fetch players multiplers from player_fpfactor_model
	$factors = $this->player_fpfactor_model->get_by('player_id', $player_id);
	$stats_array[0] = 71;
	$stats_array[1] = 65;
	$stats_array[2] = 62;
	foreach ($stats_array as $key => $stat)
	{
		$projection_stat_array[$key] = $stat_projections[$stat];
	}
	if (!$factors)
	{
		$factor_array[0] = 6;
		$factor_array[1] = 25;
		$factor_array[2] = 2;
	} else {
		$factor_array = unserialize($factors->multipliers);
	}
	$eq = ($projection_stat_array[0] * $factor_array[0]) + ($projection_stat_array[1] / $factor_array[1]) 
	+ ($projection_stat_array[2] / $factor_array[2]);
	return $eq;
}


function qb_fts($player_id, $stat_projections)
{
		//((S_32 * 4) + (S_38 * 6) + (S_29 / 50) + (S_35 / 25)) - (S_33 * 2)
		//fetch players multiplers from player_fpfactor_model
	$factors = $this->player_fpfactor_model->get_by('player_id', $player_id);
	$stats_array[0] = 32;
	$stats_array[1] = 38;
	$stats_array[2] = 29;
	$stats_array[3] = 35;
	$stats_array[4] = 33;
	foreach ($stats_array as $key => $stat)
	{
		$projection_stat_array[$key] = $stat_projections[$stat];
	}
	if (!$factors)
	{
		$factor_array[0] = 3;
		$factor_array[1] = 6;
		$factor_array[2] = 50;
		$factor_array[3] = 25;
		$factor_array[4] = null;
	} else {
		$factor_array = unserialize($factors->multipliers);
	}
	$final_factor = (is_null($factor_array[4])) ? $projection_stat_array[4] : $projection_stat_array[4] * $factor_array[4];
	$eq = (($projection_stat_array[0] * $factor_array[0]) + ($projection_stat_array[1] * $factor_array[1]) 
		+ ($projection_stat_array[2] / $factor_array[2]) + ($projection_stat_array[3] / $factor_array[3]))
	- $final_factor;
	return $eq;
}


function lb_fts($player_id, $stat_projections)
{
		//((S_32 * 4) + (S_38 * 6) + (S_29 / 50) + (S_35 / 25)) - (S_33 * 2)
		//fetch players multiplers from player_fpfactor_model
	$factors = $this->player_fpfactor_model->get_by('player_id', $player_id);
	$stats_array[0] = 107;
	$stats_array[1] = 110;
	$stats_array[2] = 112;
	$stats_array[3] = 114;
	$stats_array[4] = 115;
	$stats_array[5] = 116;
	foreach ($stats_array as $key => $stat)
	{
		$projection_stat_array[$key] = $stat_projections[$stat];
	}
	if (!$factors)
	{
		$factor_array[0] = null;
		$factor_array[1] = 2;
		$factor_array[2] = 3;
		$factor_array[3] = 4;
		$factor_array[4] = 5;
		$factor_array[5] = 6;
	} else {
		$factor_array = unserialize($factors->multipliers);
	}
	$eq = ($projection_stat_array[0]) + 
	($projection_stat_array[1] * $factor_array[1]) + 
	($projection_stat_array[2] / $factor_array[2]) + 
	($projection_stat_array[3] / $factor_array[3]) +
	($projection_stat_array[4] / $factor_array[4]) +
	($projection_stat_array[5] / $factor_array[5]);
	return $eq;
}


function dl_fts($player_id, $stat_projections)
{
		//((S_32 * 4) + (S_38 * 6) + (S_29 / 50) + (S_35 / 25)) - (S_33 * 2)
		//fetch players multiplers from player_fpfactor_model
	$factors = $this->player_fpfactor_model->get_by('player_id', $player_id);
	$stats_array[0] = 14;
	$stats_array[1] = 17;
	$stats_array[2] = 19;
	$stats_array[3] = 21;
	$stats_array[4] = 22;
	$stats_array[5] = 23;
	foreach ($stats_array as $key => $stat)
	{
		$projection_stat_array[$key] = $stat_projections[$stat];
	}
	if (!$factors)
	{
		$factor_array[0] = null;
		$factor_array[1] = 2;
		$factor_array[2] = 3;
		$factor_array[3] = 4;
		$factor_array[4] = 5;
		$factor_array[5] = 6;
	} else {
		$factor_array = unserialize($factors->multipliers);
	}
	$eq = ($projection_stat_array[0]) + 
	($projection_stat_array[1] * $factor_array[1]) + 
	($projection_stat_array[2] / $factor_array[2]) + 
	($projection_stat_array[3] / $factor_array[3]) +
	($projection_stat_array[4] / $factor_array[4]) +
	($projection_stat_array[5] / $factor_array[5]);
	return $eq;
}


function db_fts($player_id, $stat_projections)
{
		//((S_32 * 4) + (S_38 * 6) + (S_29 / 50) + (S_35 / 25)) - (S_33 * 2)
		//fetch players multiplers from player_fpfactor_model
	$factors = $this->player_fpfactor_model->get_by('player_id', $player_id);
	$stats_array[0] = 2;
	$stats_array[1] = 5;
	$stats_array[2] = 7;
	$stats_array[3] = 9;
	$stats_array[4] = 10;
	$stats_array[5] = 11;
	foreach ($stats_array as $key => $stat)
	{
		$projection_stat_array[$key] = $stat_projections[$stat];
	}
	if (!$factors)
	{
		$factor_array[0] = null;
		$factor_array[1] = 2;
		$factor_array[2] = 3;
		$factor_array[3] = 4;
		$factor_array[4] = 5;
		$factor_array[5] = 6;
	} else {
		$factor_array = unserialize($factors->multipliers);
	}
	$eq = ($projection_stat_array[0]) + 
	($projection_stat_array[1] * $factor_array[1]) + 
	($projection_stat_array[2] / $factor_array[2]) + 
	($projection_stat_array[3] / $factor_array[3]) +
	($projection_stat_array[4] / $factor_array[4]) +
	($projection_stat_array[5] / $factor_array[5]);
	return $eq;
}

function calculate_string( $string )    {
	if ($string == '()')
	{
		return 0;
	}
	error_reporting(1);
	$string = trim($string);
	$string = preg_replace('/[^0-9\+\-\*\/\(\) ]/i', '', $string);
	$compute = create_function('', 'return (' . $string . ');' );
	return 0 + $compute();

		//return ($string)*1;
}

function projections($player_id, $stat_id, $mult_value = 10)
{
	$base_year = 2012;
	$end_year = $base_year - 3;
	$multipliers_count = 0;
	$multipliers = array(5, 3, 1.5, .5);



	$player_info = $this->player_model->get($player_id);
	$player_stat = $this->stat_model->get_many_by('position_id', $player_info->position);
	$stat_info = $this->stat_model->get($stat_id);
	$stats = $this->player_year_model->get_projection($player_id, $stat_id);
	$stat_work = array();
	foreach ($stats as $stat)
	{
		$stat_work[$stat->year] = $stat->value; 
	}

	$projection_result = 0;
	for ($year = $base_year; $year>=$end_year; $year--)
	{
		if (isset($stat_work[$year]))
		{
			$projection_result += $stat_work[$year] * $multipliers[$multipliers_count];				
		}
		$multipliers_count++;

	}
	return round($projection_result / $mult_value);

}

function update_multipliers()
{
	$input = $this->input->post();
		//var_dump($input);die();
	if ($this->input->post())
	{
		$avail_stats = $this->stat_model->get_many_by('position_id', $this->input->post('position_id'));

		if ($this->input->post('update_all'))
		{
			$this->form_validation->set_rules('all_value', "All", 'trim|required|numeric|xss_clean');
		} else {
			foreach ($avail_stats as $stat)
			{
				if ($stat->projection == 'projection_formula') 
				{
					$this->form_validation->set_rules('stat['.$stat->id.']', $stat->short_name, 'trim|required|numeric|xss_clean');

				}
			}
		}


		if ($this->form_validation->run() == true)
		{
			if ($this->input->post('update_all'))
			{
				foreach ($avail_stats as $stat)
				{
					if ($stat->projection == 'projection_formula') 
					{
						$to_save = array();
						$to_save['player_id'] = $this->input->post('player_id');
						$to_save['stat_id'] = $stat->id;
						$multi_check = $this->player_multiplier_model->get_by($to_save);
						$to_save['multiplier'] = $this->input->post('all_value');
						if ($multi_check)
						{
							$this->player_multiplier_model->update($multi_check->id, $to_save);
						} else {
							$this->player_multiplier_model->insert($to_save);
						}
					}
				}
			} else {
				foreach ($this->input->post('stat') as $stat_id => $multi)
				{
					$to_save = array();
					$to_save['player_id'] = $this->input->post('player_id');
					$to_save['stat_id'] = $stat_id;
					//
					//check if stat exists
					$multi_check = $this->player_multiplier_model->get_by($to_save);
					$to_save['multiplier'] = $multi;
					if ($multi_check)
					{
						$this->player_multiplier_model->update($multi_check->id, $to_save);
					} else {
						$this->player_multiplier_model->insert($to_save);
					}
				}
			}
		}
	}
	return true;
}

} //end of class