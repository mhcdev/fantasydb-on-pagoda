<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stat extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('team_model');
		$this->load->model('player_year_model');
		$this->load->model('player_model');
		$this->load->model('stat_model');
		$this->load->model('position_model');
		$this->load->model('stat_grade_model');
	}

	function index()
	{
		$this->view_data['positions'] = $this->position_model->get_all();
		$this->view_data['stats'] = array();
		if ($this->input->get('position_id'))
		{
			$this->view_data['positions'] = $this->position_model->get_all();
			$this->view_data['stats'] = $this->stat_model->order_by('sort')->get_many_by('position_id', $this->input->get('position_id'));
			$this->view_data['position_info'] = $this->position_model->get($this->input->get('position_id'));
		}
	}

	function grade_edit($stat_id)
	{
		$this->layout_view = 'ajax';
		var_dump($stat_id);
		var_dump($this->input->post());
		$to_save = array();
		$to_save['stat_id'] = $stat_id;
		$to_save['grade'] = $this->input->post('pk');
		$grade_info = $this->stat_grade_model->get_by($to_save);

		$to_save[$this->input->post('name')] = $this->input->post('value');

		if ($grade_info)
		{
			$this->stat_grade_model->update($grade_info->id, $to_save);
		} else {
			$this->stat_grade_model->insert($to_save);
		}	
		return true;
	}

	function edit($stat_id)
	{
		$this->view_data['stat_info'] = $this->stat_model->get($stat_id);
		$this->view_data['stats'] = $this->stat_model->get_many_by('position_id', $this->view_data['stat_info']->position_id);
		$this->view_data['stat_info']->alt_eq_exp = unserialize($this->view_data['stat_info']->alt_eq);
		if ($this->input->post('projection_equation') != 'projection_formula')
		{
			$this->view_data['alt_eq'] = unserialize($this->view_data['stat_info']->alt_eq);
			//var_dump($this->view_data['alt_eq']);
		}
		$this->view_data['positions'] = $this->position_model->get_all();
		$this->view_data['position_info'] = $this->position_model->get($this->view_data['stat_info']->position_id);
		if ($this->input->post())
		{
			//var_dump($this->input->post());die();
			$this->form_validation->set_rules('name', "Full name", 'trim|xss_clean');
			$this->form_validation->set_rules('short_name', "Short name", 'trim|required|xss_clean');
			$this->form_validation->set_rules('yearly_stat', "Yearly Stat", 'trim|required|xss_clean');
			$this->form_validation->set_rules('projection_stat', "Projection Stat", 'trim|required|xss_clean');
			$this->form_validation->set_rules('projection_equation', " Use Main Projection Equation", 'trim|required|xss_clean');
			if ($this->form_validation->run() == true)
			{
				$to_save = array();
				$to_save['name'] = $this->input->post('name');
				$to_save['short_name'] = $this->input->post('short_name');
				$to_save['yearly_stat'] = $this->input->post('yearly_stat');
				$to_save['projection_stat'] = $this->input->post('projection_stat');
				$to_save['projection'] = ($this->input->post('projection_equation') == 1) ? 'projection_formula' : null;
				$to_save['alt_eq'] = serialize($this->input->post('alt_eq'));
				$new = $this->stat_model->update($stat_id, $to_save);
				if ($new)
				{
					redirect('stat?position_id='.$this->view_data['stat_info']->position_id);
				}
			}
		}
	}

	function add($position_id)
	{
		$this->view_data['stats'] = $this->stat_model->get_many_by('position_id', $position_id);
		$this->view_data['positions'] = $this->position_model->get_all();
		$this->view_data['position_info'] = $this->position_model->get($position_id);
		if ($this->input->post())
		{
			$this->form_validation->set_rules('name', "Full name", 'trim|xss_clean');
			$this->form_validation->set_rules('short_name', "Short name", 'trim|required|xss_clean');
			$this->form_validation->set_rules('yearly_stat', "Yearly Stat", 'trim|required|xss_clean');
			$this->form_validation->set_rules('projection_stat', "Projection Stat", 'trim|required|xss_clean');
			$this->form_validation->set_rules('projection_equation', " Use Main Projection Equation", 'trim|required|xss_clean');
			if ($this->form_validation->run() == true)
			{
				$to_save = array();
				$to_save['name'] = $this->input->post('name');
				$to_save['position_id'] = $this->view_data['position_info']->id;
				$to_save['short_name'] = $this->input->post('short_name');
				$to_save['yearly_stat'] = $this->input->post('yearly_stat');
				$to_save['projection_stat'] = $this->input->post('projection_stat');
				$to_save['projection'] = ($this->input->post('projection_equation') == 1) ? 'projection_formula' : null;
				$to_save['alt_eq'] = serialize($this->input->post('alt_eq'));
				$new = $this->stat_model->insert($to_save);
				if ($new)
				{
					redirect('stat?position_id='.$position_id);
				}
			}
		}
	}


	function grades($stat_id)
	{
		//get grades for stat
		$this->view_data['stat_info'] = $this->stat_model->get($stat_id);
		$grades = $this->stat_grade_model->get_many_by('stat_id', $stat_id);

		foreach ($grades as $grade)
		{
			$this->view_data['grades'][$grade->grade] = $grade;
 		}
	}
	function make_string()
	{
		$x = 'a:2:{s:4:"vars";a:2:{i:0;i:44;i:1;i:43;}s:8:"equation";s:13:"var_0 / var_1";}';
		var_dump(unserialize($x));
		$to_save = array();
		$to_save['vars'][] = 47;
		$to_save['vars'][] = 55;
		$to_save['equation'] = 'var_0 + var_1';
		var_dump($to_save);
		echo serialize($to_save);
	}

	function xx($id)
	{
		$x = $this->stat_model->get($id);
		$eq = $x->alt_eq;
		var_dump($x);
		//var_dump($eq);

		$y = unserialize($x->alt_eq);
		var_dump($y);
	}

	public function sort()
    {
      $this->layout_view = 'ajax';
      $order_array = $this->input->post('row');
      foreach ($order_array as $position => $item)
      {
        //$member = $this->$curr_model->get_by_id($item);
        $to_save['sort'] = $position;
        $this->stat_model->update($item, $to_save);
      }
      return true;
    }



/*
	function view($id, $year = null)
	{
		$this->view_data['team_info'] = $this->team_model->get($id);
		if (!is_null($year))
		{
			$search_array = array();
			$search_array['year'] = $year;
			$search_array['team_id'] = $id;
			$players = $this->player_year_model->get_many_by($search_array);
			foreach ($players as &$player)
			{
				$player->player_info = $this->player_model->get($player->player_id);
				//$player->team_info= $this->team_model->get($player->team_id);
			}
			$this->view_data['viewing_year'] = $year;
			$this->view_data['players'] = $players;
		} else {

			$avail_years = $this->player_year_model->available_team_years($id);
			foreach ($avail_years as $year)
			{
				$search_array = array();
				$search_array['year'] = $year->year;
				$search_array['team_id'] = $id;
				$players = $this->player_year_model->get_many_by($search_array);
				foreach ($players as &$player)
				{
					$player->player_info = $this->player_model->get($player->player_id);
				//$player->team_info= $this->team_model->get($player->team_id);
				}
				$team_players[$year->year] = $players;
			}
			$this->view_data['years'] = $team_players;
		}
		//var_dump($players);
		//get availble years for team
	}
	*/

} //end of class