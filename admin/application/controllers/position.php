<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Position extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('team_model');
		$this->load->model('player_year_model');
		$this->load->model('player_model');
		$this->load->model('stat_model');
		$this->load->model('position_model');
	}

	function index()
	{
		$this->view_data['positions'] = $this->position_model->get_all();
	}

	function view($position_id)
	{
		$this->view_data['position_info'] = $this->position_model->get($position_id);
		$this->view_data['players'] = $this->player_model->get_many_by('position', $position_id);
	}
} //end of class