<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_card extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('team_model');
		$this->load->model('player_model');
		$this->load->model('player_year_model');
		$this->load->model('position_model');
		$this->load->model('stat_model');
		$this->load->model('player_year_stat_model');
		$this->load->model('player_multiplier_model');
		$this->load->model('player_fpfactor_model');
		$this->load->model('stat_grade_model');
	}

	function index()
	{
		//list all stats
		$this->view_data['stats'] = $this->stat_model->get_all();
	}

	function add()
	{

	}

	function edit()
	{

	}

} //end of class