<?php if ( ! defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

class Importer extends MY_Controller {

	function __construct() {
		parent::__construct();
		//die();
		$this->load->model( 'nfl_defensive_back_model' );
		$this->load->model( 'nfl_defensive_linemen_model' );
		$this->load->model( 'nfl_quarterback_model' );
		$this->load->model( 'nfl_running_back_model' );
		$this->load->model( 'nfl_wide_receiver_model');
		$this->load->model( 'nfl_tight_end_model');
		$this->load->model( 'nfl_kicker_model' );
		$this->load->model( 'nfl_linebacker_model' );
		$this->load->model( 'team_model' );
		$this->load->model( 'player_model' );
		$this->load->model( 'player_year_model' );
		$this->load->model( 'stat_model' );
		$this->load->model( 'player_year_stat_model' );
		$this->load->model( 'nfl_profile_model' );
		$this->load->model( 'nfl_team_profile_work_model' );
		$this->load->model('auction_value_model');
		$this->load->model('nfl_dst_model');
		$this->load->model('team_year_model');
		$this->load->model('team_year_stat_model');
		$this->load->model('qb_multipler_model');
		$this->load->model('rb_multipler_model');
		$this->load->model('wr_multipler_model');
		$this->load->model('player_multiplier_model');
	}


	function mwork()
	{
		//player_multiplier_model
		//$multis = $this->qb_multipler_model->get_all();
		$multis = $this->wr_multipler_model->get_all();
		//var_dump($multis);
		//return true;
		//echo count($multis);
		foreach ($multis as $multi)
		{
			//find player id
			$to_find = array();
			$split_name = explode(',', $multi->name_comma);
			$to_find['last_name'] = trim($split_name[0]);
			$to_find['first_name'] = trim($split_name[1]);
			$to_find['position'] = 6;
			$player_info = $this->player_model->get_many_by($to_find);
			if (!$player_info)
			{
				var_dump('error');
				var_dump($to_find);
				continue;
			} elseif (count($player_info)>1) {
				var_dump('to_many');
				continue;
			} else {
				$player_info = (Array)$player_info;
				$player_info = $player_info[0];
				//var_dump($player_info);
			}
			//continue;
			
			foreach ($multi as $key => $value)
			{
				//check if stat exsits;
				$to_find = array();
				$to_find['position_id'] = 6;
				/*
				if ($key == 'Att')
				{
					$key = 'PaAtt';
				}
				if ($key == 'Rush')
				{
					$key = 'RuAtt';
				}
				*/
				$to_find['short_name'] = $key;
				$stat_info = $this->stat_model->get_by($to_find);
				//var_dump($stat_info);
				
				if ($stat_info)
				{
					//var_dump($stat_info);
					$to_save = array();
					$to_save['player_id'] = $player_info->id;
					$to_save['stat_id'] = $stat_info->id;

					$multi_check = $this->player_multiplier_model->get_by($to_save);

					$to_save['multiplier'] = $value;


					if ($multi_check)
					{
						$new = $this->player_multiplier_model->update($multi_check->id, $to_save);
					} else {
						$new = $this->player_multiplier_model->insert($to_save);
					}
					var_dump($new);
					//var_dump($to_save);
				}
				

				//var_dump($key);
			}

			//var_dump($to_save);
			echo '<hr>';
		}
	}

	function auctions()
	{
		$i = 0;

		$auctions = $this->auction_value_model->get_many_by('team', 0);
		foreach ($auctions as $auction)
		{
			var_dump($auction);
			$player = $auction->player;
			$pos = strpos($player, '. ');
			//var_dump($pos);
			$player = substr($player, $pos+1);
			//var_dump($player);
			$pos = strpos($player, ', ');
			if ($pos)
			{
				$player = substr($player, 0, $pos);
			}
			
			//echo 'aa';
			//var_dump($player);
			$player= trim($player);
			$player = explode(' ', $player);
			//var_dump($player);
			if (count($player)>2) 
			{
				$player[1] = $player[1].' '.$player[2]; 
			}
			$to_find = array();
			$to_find['first_name'] = $player[0];
			$to_find['last_name'] = $player[1];
			var_dump($player);
			if ($to_find['first_name'] == 'Cam' && $to_find['last_name'] == 'Newton')
			{
				$player_lookup = $this->player_model->get(1697);
			} elseif ($to_find['first_name'] == 'Alex' && $to_find['last_name'] == 'Smith'){
				$player_lookup = $this->player_model->get(1732);
			} elseif ($to_find['first_name'] == 'Chris' && $to_find['last_name'] == 'Johnson'){
				$player_lookup = $this->player_model->get(1983);
			} else {
				$player_lookup = $this->player_model->get_by($to_find);
			}

			$to_save = array();
			$to_save['auction_value'] = substr($auction->value, strpos($auction->value, '$')+1);
			var_dump($to_save);
			$x = $this->player_model->update($player_lookup->id, $to_save);
			var_dump($x);
			if (count($player_lookup)>1)
			{
				var_dump($player_lookup);
				$i++;
			}
			//var_dump($player);
			echo '<hr/>';
		}
		var_dump($i);
		die();
	}

	function team()
	{
		die();
		$teams = $this->nfl_team_profile_work_model->get_all();
		foreach ($teams as $team)
		{
			var_dump($team);
			$curr_team = $this->team_model->as_array()->get_team_ci($team->name)->get_all();
			$curr_team = $curr_team[0];
			//var_dump($curr_team);die();
			$to_save = array();
			$to_save['city'] = trim($team->city);
			$to_save['bye_week'] = $team->bye_week;
			$to_save['profile'] = $team->profile;
			$to_save['rank'] = $team->rank;
			$upd = $this->team_model->update($curr_team['id'], $to_save);
			var_dump($upd);
		}
		die();
	}
	function profile()
	{
		die();
		$count = 1;
		$position_array = array('QB' => 3,
			'RB' => 4,
			'WR' => 5,
			'TE' => 6,
			'K' => 7,
			'DL' => 2,
			'LB' => 8,
			'DB' => 1);
		$profiles = $this->nfl_profile_model->get_all();
		foreach ($profiles as $profile)
		{
			//var_dump($profile);
			if ($profile->pos == 'D/ST') continue;
			$to_save = array();
			$to_save['pos'] = $profile->pos;
			$rank = trim(substr($profile->name, 0, strpos($profile->name, ')') + 1));
			$temp_name = trim(substr($profile->name, strpos($profile->name, ')') + 1));
			$rank = trim(str_replace('(','', $rank));
				$to_save['rank'] = trim(str_replace(')','', $rank));


				$words = explode(' ', $temp_name);
				$team = array_pop($words);

				$full_name = trim(substr($temp_name, 0, strrpos($temp_name, ' ') + 1));
			//var_dump($full_name);
				$exploded_name = explode(',', $full_name);
			//var_dump($exploded_name);
				$to_save['first_name'] = trim($exploded_name[1]);
				$to_save['last_name'] = trim($exploded_name[0]);
				$to_save['team_name'] = (strlen(trim(substr($temp_name, strrpos($temp_name, ' ') + 1))) == 0) ? 'ERROR' : trim(substr($temp_name, strrpos($temp_name, ' ') + 1)); 
				$to_save['age'] = trim(str_replace('AGE:','', $profile->age));
				$to_save['bye_week'] = trim(str_replace('BYE WEEK:','', $profile->bye));
			//try to match player
				$to_find = array();
				$to_find['first_name'] = $to_save['first_name'];
				$to_find['last_name'] = $to_save['last_name'];
				$to_find['position'] = $position_array[$profile->pos];
				$name_lookup = $this->player_model->as_array()->get_many_by($to_find);
				if ($name_lookup && count($name_lookup) == 1)
				{
					$to_save['player_id'] = $name_lookup[0]['id'];
				} else {
					$to_save['player_id'] = 'ERROR';
				}

				$team_info = $this->team_model->as_array()->get_many_by('name', $to_save['team_name']);
				if ($team_info && count($team_info) == 1)
				{
					$to_save['team_id'] = $team_info[0]['id'];
				} else {
					$to_save['team_id'] = 40;
				}
				$to_save['profile'] = $profile->profile;

			/*
			if ($to_save['team_id'] == 'ERROR')
			{
			var_dump($to_save);
			}
			*/

			if ($to_save['player_id'] == 'ERROR')
			{
				echo $count++;
				//var_dump($to_find);
				//var_dump($to_save);
				
				$to_update = array();
				$to_update['first_name'] = $to_save['first_name'];
				$to_update['last_name'] = $to_save['last_name'];
				$to_update['profile'] = $to_save['profile'];
				$to_update['rank'] = $to_save['rank'];
				$to_update['age'] = $to_save['age'];
				$to_update['position'] = $position_array[$profile->pos];
				$to_update['profile'] = $to_save['profile'];
				$to_update['bye_week'] = $to_save['bye_week'];

				$new_player = $this->player_model->insert($to_update);
				var_dump($to_update);
				
				if ($new_player)
				{


					$year_update = array();
					$year_update['team_id'] = $to_save['team_id'];
					$year_update['player_id'] = $new_player;
					$year_update['year'] = 2013;
					$year_update['pos'] = $profile->pos;

					$x = $this->player_year_model->insert($year_update);
					var_dump($x);


					var_dump($year_update);
				} else {
					echo 'error<BR>';
				}
				echo '<hr/><Br/>';


			}
			

		//update profiles

		/*
		if ($to_save['player_id'] != 'ERROR')
		{
			//var_dump($to_save);
			$to_update = array();
			$to_update['profile'] = $to_save['profile'];
			$to_update['bye_week'] = $to_save['bye_week'];
			$to_update['age'] = $to_save['age'];
			$to_update['rank'] = $to_save['rank'];
			$x = $this->player_model->update($to_save['player_id'], $to_update);
			var_dump($x);
			var_dump($to_update);
			echo '<hr/>';
		}
		*/
		

		//create entry in player_years
		//
		
		/*
		if ($to_save['player_id'] != 'ERROR')
		{
			$to_update = array();
			$to_update['team_id'] = $to_save['team_id'];
			$to_update['player_id'] = $to_save['player_id'];
			$to_update['year'] = 2013;
			$to_update['pos'] = $profile->pos;

			$x = $this->player_year_model->insert($to_update);
			var_dump($x);
			var_dump($to_update);
			echo '<hr/>';
		}
		*/

		} //end of foreach


	} //end of function
	function index() {
		die();

/*
		$rows = $this->nfl_defensive_back_model->get_all();
		$position_id = 1;
*/

/*
		$rows = $this->nfl_defensive_linemen_model->get_all();
		$position_id = 2;
*/

/*
		$rows = $this->nfl_quarterback_model->get_all();
		$position_id = 3;
*/

	/*
		$rows = $this->nfl_running_back_model->get_all();
		$position_id = 4;
*/
		
		/*
		$rows = $this->nfl_wide_receiver_model->get_all();
		//var_dump($rows);die();
		$position_id = 5;
		*/
		
		
		/*
		$rows = $this->nfl_tight_end_model->get_all();
		$position_id = 6;
*/
		
		/*
		$rows = $this->nfl_kicker_model->get_all();
		$position_id = 7;
		 */
		
		//$rows = $this->nfl_linebacker_model->get_all();
		//$position_id = 8;
		//
		$rows = $this->nfl_dst_model->get_all();
		$position_id = 9;

		//die();
		//var_dump($rows);
		foreach ($rows as $row)
		{
			//var_dump($row);
			$to_save = array();
			$to_save['position'] = $position_id;
			//$to_save['full_name'] = trim($row->Name);
			$split_name = explode(' ', trim($row->Team));
			if (count($split_name)>2)
			{
				$to_save['city'] = $split_name[0].' '.$split_name[1];
				$to_save['name'] = $split_name[2];
			} else {
				$to_save['city'] = $split_name[0];
				$to_save['name'] = $split_name[1];
			}
			$team_info = $this->team_model->get_by('name', $to_save['name']);

			$to_save = array();
			$to_save['team_id'] = $team_info->id;
			$to_save['year'] = $row->Yr;
			$to_save['pos'] = $position_id;
			$year_info = $this->team_year_model->get_by($to_save);
			if (!$year_info)
			{
				$new_year_stat = $this->team_year_model->insert($to_save);
			} else {
				$new_year_stat = $year_info->id;
			}

			foreach ($row as $key => $value)
			{

				//check if key is valid metric
				$to_find = array();
				$to_find['short_name'] = trim($key);
				$to_find['position_id'] = trim($position_id);
				$stat_info = $this->stat_model->get_by($to_find);
				var_dump($stat_info);
				
				if ($stat_info)
				{
					$to_save = array();
					$to_save['team_year_id'] = $new_year_stat;
					$to_save['stat_id'] = trim($stat_info->id);
					//
					$year_stat_info = $this->team_year_stat_model->get_by($to_save);
					$to_save['value'] = $value;
					//var_dump($year_stat_info);die();

					if (!$year_stat_info)
					{
						$new_stat = $this->team_year_stat_model->insert($to_save);
					} else {
						//$new_stat = $year_stat_info->id;
						$new_stat = $this->team_year_stat_model->update($year_stat_info->id, $to_save);
					}
					var_dump($new_stat);
				}
				
			}
			ob_flush();


			//$to_save['last_name'] = trim($split_name[0]);
			//$to_save['first_name'] = trim($split_name[1]);
			var_dump($to_save);
			echo '<hr>';
		}
		//die();
		//
		//get 1st row
		//pass through create_stats & team_work


		//$row = (array) $rows[0];
		//$this->create_stats($row, $position_id);
		//die();

		/*
		foreach ($rows as $key => $row)
		{
			$teams[] = $row->Team;
			$team_id = $this->team_work($row->Team);
		}
		var_dump($teams);
		*/

		die();
		foreach ($rows as $key => $row)
		{
			$to_save = array();
			$to_save['position'] = $position_id;
			//$to_save['full_name'] = trim($row->Name);
			$split_name = explode(',', $row->Team);
			$to_save['last_name'] = trim($split_name[0]);
			$to_save['first_name'] = trim($split_name[1]);
			var_dump($to_save);
			die();
			$player_info = $this->player_model->get_by($to_save);
			//var_dump($to_save);
			var_dump($player_info);
			//ob_flush();
			//continue;
			if (!$player_info)
			{
				$new_player = $this->player_model->insert($to_save);
			} else {
				$new_player = $player_info->id;
			}
			//var_dump($new_player);
			//ob_flush();
			//continue;
			$to_save = array();
			$to_save['team_id'] = $this->team_work($row->Team);
			$to_save['year'] = trim($row->Year);
			$to_save['player_id'] = trim($new_player);
			$to_save['pos'] = trim($row->Pos);
			//var_dump($to_save);
			$year_info = $this->player_year_model->get_by($to_save);
			if (!$year_info)
			{
				$new_year_stat = $this->player_year_model->insert($to_save);
			} else {
				$new_year_stat = $year_info->id;
			}

			foreach ($row as $key => $value)
			{

				//check if key is valid metric
				$to_find = array();
				$to_find['short_name'] = trim($key);
				$to_find['position_id'] = trim($position_id);
				$stat_info = $this->stat_model->get_by($to_find);
				if ($stat_info)
				{
					$to_save = array();
					$to_save['player_year_id'] = $new_year_stat;
					$to_save['stat_id'] = trim($stat_info->id);
					//
					$year_stat_info = $this->player_year_stat_model->get_by($to_save);
					$to_save['value'] = $value;
					//var_dump($year_stat_info);die();

					if (!$year_stat_info)
					{
						$new_stat = $this->player_year_stat_model->insert($to_save);
					} else {
						//$new_stat = $year_stat_info->id;
						$new_stat = $this->player_year_stat_model->update($year_stat_info->id, $to_save);
					}
					var_dump($new_stat);
				}
			}
			ob_flush();
			//$teams[] = $row->Team;
			//$team_id = $this->team_work($row->Team);
		} //end of foreach

		//$teams = array_unique($teams);
		//var_dump($teams);

	}

	function create_stats( $row, $position_id ) {
		$to_unset = array();
		$to_unset[] = 'Pos';
		$to_unset[] = 'Name';
		$to_unset[] = 'Year';
		$to_unset[] = 'Yr';
		$to_unset[] = 'id';
		$to_unset[] = 'Team';
		$to_unset[] = 'name_comma';
		foreach ( $to_unset as $unset ) {
			unset( $row[$unset] );
		}
		var_dump( $row );
		$keys = array_keys( $row );
		//var_dump($keys);die();
		foreach ( $keys as $key ) {
			$to_save = array();
			$to_save['short_name'] = $key;
			$to_save['position_id'] = $position_id;
			$stat_info = $this->stat_model->get_by( $to_save );
			if ( !$stat_info ) {
				$new_stat = $this->stat_model->insert( $to_save );
			} else {
				$new_stat = $stat_info->id;
			}
			var_dump( $to_save );
			var_dump( $new_stat );
		}
	}

	function team_work( $team ) {
		$team_info = $this->team_model->get_by( 'short_name', $team );
		if ( $team_info ) {
			return $team_info->id;
		} else {
			$to_save = array();
			$to_save['sport_id'] = 1;
			$to_save['short_name'] = $team;
			$new = $this->team_model->insert( $to_save );
			return $team;
		}
	}
}
