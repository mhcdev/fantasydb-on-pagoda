<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('team_model');
		$this->load->model('player_year_model');
		$this->load->model('player_model');

	}

	function index()
	{
		$this->view_data['teams'] = $this->team_model->get_all();
		foreach ($this->view_data['teams'] as &$team)
		{
			$team->avail_years = $this->player_year_model->available_team_years($team->id);
		}
	}

	function view($id, $year = null)
	{
		$this->view_data['team_info'] = $this->team_model->get($id);
		if (!is_null($year))
		{
			$search_array = array();
			$search_array['year'] = $year;
			$search_array['team_id'] = $id;
			$players = $this->player_year_model->get_many_by($search_array);
			foreach ($players as &$player)
			{
				$player->player_info = $this->player_model->get($player->player_id);
				//$player->team_info= $this->team_model->get($player->team_id);
			}
			$this->view_data['viewing_year'] = $year;
			$this->view_data['players'] = $players;
		} else {

			$avail_years = $this->player_year_model->available_team_years($id);
			foreach ($avail_years as $year)
			{
				$search_array = array();
				$search_array['year'] = $year->year;
				$search_array['team_id'] = $id;
				$players = $this->player_year_model->get_many_by($search_array);
				foreach ($players as &$player)
				{
					$player->player_info = $this->player_model->get($player->player_id);
				//$player->team_info= $this->team_model->get($player->team_id);
				}
				$team_players[$year->year] = $players;
			}
			$this->view_data['years'] = $team_players;
		}
		//var_dump($players);
		//get availble years for team
	}

	function edit($team_id)
	{
		$this->view_data['team_info'] = $this->team_model->get($team_id);
		if ($this->input->post())
		{
			$this->form_validation->set_rules('name', "Full name", 'trim|xss_clean');
			$this->form_validation->set_rules('short_name', "Short name", 'trim|required|xss_clean');
			$this->form_validation->set_rules('division', "Division", 'trim|xss_clean');
			$this->form_validation->set_rules('head_coach', "Head Coach", 'trim|xss_clean');
			$this->form_validation->set_rules('rank', "Rank", 'trim|xss_clean');
			$this->form_validation->set_rules('profile', "Profile", 'trim|xss_clean');
			$this->form_validation->set_rules('offensive_coordinator', "Offensive Coordinator", 'trim|xss_clean');
			$this->form_validation->set_rules('primary_color', "Primary Stat", 'trim|xss_clean');
			$this->form_validation->set_rules('secondary_color', "Secondary Stat", 'trim|xss_clean');
			if ($this->form_validation->run() == true)
			{
				$to_save = array();
				$to_save['name'] = $this->input->post('name');
				$to_save['division'] = $this->input->post('division');
				$to_save['head_coach'] = $this->input->post('head_coach');
				$to_save['offensive_coordinator'] = $this->input->post('offensive_coordinator');
				$to_save['primary_color'] = $this->input->post('primary_color');
				$to_save['secondary_color'] = $this->input->post('secondary_color');
				$to_save['rank'] = $this->input->post('rank');
				$to_save['profile'] = $this->input->post('profile');
				$new = $this->team_model->update($team_id, $to_save);
				if ($new)
				{
					redirect('team');
				}
			}
		}
	}

} //end of class