<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roster extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('team_model');
		$this->load->model('player_model');
		$this->load->model('player_year_model');
		$this->load->model('stat_model');
		$this->load->model('player_year_stat_model');
		$this->load->model('player_multiplier_model');
	}


	function last_season_players($team_id = null)
	{
		$proj_year = 2013;
		$is_projection = 1;
		if ($this->input->post())
		{
			foreach ($this->input->post('player') as $player_id => $player)
			{
				//var_dump($player);
				if (isset($player['is_active']) && $player['is_active'] == 1)
				{
					//var_dump($player_id .' is active');
					//make sure player is in player_year_model w/ right team
					$to_find = array();
					$to_find['year'] = $proj_year;
					$to_find['player_id'] = $player_id;
					$check_player = $this->player_year_model->get_by($to_find);
					if (!$check_player)
					{
						$to_save = array();
						$to_save['team_id'] = $player['team_id'];
						$to_save['player_id'] = $player_id;
						$to_save['year'] = $proj_year;
						$to_save['is_projection'] = 1;
						$new = $this->player_year_model->insert($to_save);
					} else {
						$to_save = array();
						$to_save['team_id'] = $player['team_id'];
						$to_save['player_id'] = $player_id;
						$to_save['year'] = $proj_year;
						$to_save['is_projection'] = 1;
						$new = $this->player_year_model->update($check_player->id, $to_save);
					}
				} else {
					//var_dump($player_id .' is not active');
					//check if player is in player_year_model for $proj_year
					//if so delete it
					$to_find = array();
					$to_find['year'] = $proj_year;
					$to_find['player_id'] = $player_id;
					$check_player = $this->player_year_model->get_by($to_find);
					if ($check_player)
					{
						$delete = $this->player_year_model->delete($check_player->id);
					}
				}
			}
		}
		$this->view_data['players'] = $this->player_year_model->get_players_from_year(2012);
		foreach ($this->view_data['players'] as &$player)
		{
			$to_find = array();
			//$to_find['player_id'] = $player->id;
			//$to_find['year'] = 2013;
			$player->proj_year = $this->player_year_model->get_player_year_info($player->id, 2013);
		}
		//var_dump($this->view_data['players']);die();
		$this->view_data['teams'] = $this->team_model->get_all();
	}


} //end of class