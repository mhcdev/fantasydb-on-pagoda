<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                Teams
                <a href="<?php echo site_url('team/add');?>" class="btn btn-success btn-mini pull-right">Add Team</a>

            </div>
            <div class="box-content nopadding">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Sport</th>
                            <th>Short Name</th>
                            <th>Name</th>
                            <th>Rank</th>
                            <th style="width:210px;" >&nbsp;</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Sport</th>
                            <th>Short Name</th>
                            <th>Name</th>
                            <th>Rank</th>
                            <th style="width:210px;" >&nbsp;</th>
                        </tr>
                    </tfoot>
                    <tbody id="pages_index">
                        <?php foreach ($teams as $i => $team):?>
                        <tr class="" id="row_<?php echo $team->id;?>">
                            <td><?php echo FR_lookup_id('sport_model', $team->sport_id, 'sport');?></td>
                            <td><?php echo $team->short_name;?></td>
                            <td><?php echo $team->name;?></td>
                            <td><?php echo $team->rank;?></td>
                     <!--
                        <td class="center"> 4</td>
                        <td class="center">X</td>
                    -->
                    <td class="">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn dropdown-toggle">Years <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('team/view/'.$team->id);?>">All</a></li>
                                <?php foreach ($team->avail_years as $year):?>
                                <li><a href="<?php echo site_url('team/view/'.$team->id.'/'.$year->year);?>"><?php echo $year->year;?></a></li>
                            <?php endforeach;?>


                        </ul>
                    </div>
                    <a href="<?php echo site_url('team/edit/'.$team->id);?>" class="btn btn-info btn-mini">Edit</a>
                    <a href="#" class="btn btn-danger btn-mini delete">Delete</a>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
</div>
</div>
</div>
</div>
