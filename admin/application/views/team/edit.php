    <div class="row-fluid">
      <div class="span12">

        <?php echo validation_errors(); ?>


        <div class="box">
          <div class="box-title">
            Edit Team <?php echo $team_info->short_name;?>
          </div>
          <div class="box-content nopadding">
            <form class="form-horizontal" method="post" action="<?php echo current_url();?>">
              <div class="control-group">
                <label class="control-label" for="name">Full Name</label>
                <div class="controls">
                  <input type="text" id="name" placeholder="Full Name" name="name" value="<?php echo set_value( 'name', $team_info->name );?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="short_name">Short Name</label>
                <div class="controls">
                  <input type="text" id="short_name" placeholder="Short Name" name="short_name"  value="<?php echo set_value( 'short_name', $team_info->short_name );?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="short_name">Division</label>
                <div class="controls">
                  <input type="text" id="division" placeholder="Division" name="division"  value="<?php echo set_value( 'division', $team_info->division );?>">
                </div>
              </div>

               <div class="control-group">
                <label class="control-label" for="rank">Rank</label>
                <div class="controls">
                  <input type="text" id="rank" placeholder="Rank" name="rank"  value="<?php echo set_value( 'rank', $team_info->rank );?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="profile">Profile</label>
                <div class="controls">
                  <textarea class="span10" rows="10" id="profile" placeholder="Profile" name="profile"><?php echo set_value('profile', $team_info->profile);?></textarea>
                </div>
              </div>
   

   
              <div class="control-group">
                <label class="control-label" for="head_coach">Head Coach</label>
                <div class="controls">
                  <input type="text" id="head_coach" placeholder="Head Coach" name="head_coach"  value="<?php echo set_value( 'head_coach', $team_info->head_coach );?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="Offensive Coordinator">Offensive Coordinator</label>
                <div class="controls">
                  <input type="text" id="offensive_coordinator" placeholder="Offensive Coordinator" name="offensive_coordinator"  value="<?php echo set_value( 'offensive_coordinator', $team_info->offensive_coordinator );?>">
                </div>
              </div>


              <div class="control-group">
                <label class="control-label" for="primary_color">Primary color</label>
                <div class="controls">
                  <input type="text" class="colorpicker" id="primary_color" placeholder="" name="primary_color"  value="<?php echo set_value('primary_color', $team_info->primary_color );?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="secondary_color">Secondary color</label>
                <div class="controls">
                  <input type="text" class="colorpicker" id="secondary_color" placeholder="" name="secondary_color"  value="<?php echo set_value('secondary_color', $team_info->secondary_color );?>">
                </div>
              </div>



              <div class="control-group">
                <div class="controls">
                  <button type="submit" class="btn">Submit</button>
                </div>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
    <script>
    $(function() {
      $('.colorpicker').colorpicker()
    });
    </script>