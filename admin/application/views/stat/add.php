    <div class="row-fluid">
      <div class="span12">

        <?php echo validation_errors(); ?>

        
        <div class="box">
          <div class="box-title">
            Edit Stat for <?php echo $position_info->name;?>
          </div>
          <div class="box-content nopadding">
            <form class="form-horizontal" method="post" action="<?php echo current_url();?>">
              <div class="control-group">
                <label class="control-label" for="name">Full Name</label>
                <div class="controls">
                  <input type="text" id="name" placeholder="Full Name" name="name" value="<?php echo set_value('name');?>">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="short_name">Short Name</label>
                <div class="controls">
                  <input type="text" id="name" placeholder="Short Name" name="short_name"  value="<?php echo set_value('short_name');?>">
                </div>
              </div>

              <?php $stat_options = array(1 => 'Yes', 0 => 'No');?>
              <div class="control-group">
                <label class="control-label" for="yearly_stat">Yearly Stat</label>
                <div class="controls">
                  <select id="yearly_stat" name="yearly_stat">
                   <option value="">--</option>
                   <?php foreach ($stat_options as $option_id => $option):?>
                   <option value="<?php echo $option_id;?>" ><?php echo $option;?></option>
                 <?php endforeach;?>

               </select>
             </div>
           </div>

           <div class="control-group">
            <label class="control-label" for="projection_stat">Projection Stat</label>
            <div class="controls">
              <select id="projection_stat" name="projection_stat">
                <option value="">--</option>
                <?php foreach ($stat_options as $option_id => $option):?>
                <option value="<?php echo $option_id;?>"  ><?php echo $option;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>

        <div class="control-group">
          <label class="control-label" for="inputEmail">Use Main Projection Equation</label>
          <div class="controls">
            <select id="projection_equation" name="projection_equation" >
              <option value="">--</option>
              <?php foreach ($stat_options as $option_id => $option):?>
              <option value="<?php echo $option_id;?>" ><?php echo $option;?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <?php $eq_opearations = array('-' => '-', '+' => '+', '/' => '/', '*'=>'*');?>
      <div class="control-group" id="alt_eq_container">

        <label class="control-label" for="projection_stat">Alternate Equation</label>
        <div class="controls">
          (
          <select id="var_0" name="alt_eq[]" class="span2">
            <option value="">--</option>
            <?php foreach ($stats as $stat):?>
            <option value="<?php echo $stat->id;?>"  ><?php echo $stat->short_name;?></option>
          <?php endforeach;?>
        </select>

        <select id="eq_operator" name="alt_eq[]" class="span1">
          <option value="">--</option>
          <?php foreach ($eq_opearations as $option):?>
          <option value="<?php echo $option;?>"  ><?php echo $option;?></option>
        <?php endforeach;?>
      </select>

      <select id="var_1" name="alt_eq[]" class="span2">
        <option value="">--</option>
        <?php foreach ($stats as $stat):?>
        <option value="<?php echo $stat->id;?>"  ><?php echo $stat->short_name;?></option>
      <?php endforeach;?>
    </select>
    )

    <select id="eq_operator_2" name="alt_eq[]" class="span1">
      <option value="">--</option>
      <?php foreach ($eq_opearations as $option):?>
      <option value="<?php echo $option;?>"  ><?php echo $option;?></option>
    <?php endforeach;?>
  </select>

  <input type="text" id="factor" placeholder="Factor" name="alt_eq[factor]"  value="" class="span2">



</div>
</div>



<div class="control-group">
  <div class="controls">
    <button type="submit" class="btn">Submit</button>
  </div>
</div>
</form>

</div>
</div>
</div>
</div>