<?php 
error_reporting(0);
$grade_array = array('A+', 'A', 'A-', 
                           'B+', 'B', 'B-',
                           'C+', 'C', 'C-',
                           'D+', 'D', 'D-',
                           'F'); ?>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                Grades for <?php echo $stat_info->short_name;?>
            </div>
            <div class="box-content nopadding">
                <table class="table table-striped table-bordered" id="sortable">
                    <thead>
                        <tr>
                            <th>Grade</th>
                            <th>Min</th>
                            <th>Max</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Grade</th>
                            <th>Min</th>
                            <th>Max</th>
                        </tr>
                    </tfoot>
                    <tbody id="pages_index">
                        <?php foreach ($grade_array as $i => $grade):?>
                        <tr class="" id="row_<?php echo $i;?>">
                            <td><?php echo $grade;?></td>
                            <td><a href="#" class="editable" id="min" data-type="text" data-pk="<?php echo $grade;?>" data-url="<?php echo site_url('stat/grade_edit/'.$stat_info->id);?>" data-title="Enter min value"><?php echo $grades[$grade]->min;?></a></td>
                            <td><a href="#" class="editable" id="max" data-type="text" data-pk="<?php echo $grade;?>" data-url="<?php echo site_url('stat/grade_edit/'.$stat_info->id);?>" data-title="Enter max value"><?php echo $grades[$grade]->max;?></a></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
</div>
</div>
</div>

<script>
$(document).ready(function() {
    $('.editable').editable();
});
</script>