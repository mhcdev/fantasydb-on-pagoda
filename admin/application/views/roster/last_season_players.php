<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <form class="form" method="post" action="<?php echo current_url();?>">

                <div class="box-title">
                    Last Season Players
                    <input type="submit" value="Update" class="pull-right">
                </div>
                <div class="box-content nopadding">

                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Position</th>
                                <th>Team</th>
                                <th>2013 Info</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Position</th>
                                <th>Team</th>
                                <th>2013 Info</th>
                            </tr>
                        </tfoot>
                        <tbody id="pages_index">
                            <?php foreach ($players as $i => $player):?>
                            <tr class="" id="row_<?php echo $player->id;?>">
                                <td><?php echo $player->first_name;?></td>
                                <td><?php echo $player->last_name;?></td>
                                <td><?php echo $player->pos;?></td>
                                <td><?php echo $player->team_name;?></td>
                     <!--
                        <td class="center"> 4</td>
                        <td class="center">X</td>
                    -->
                    <td class="">

                     <div class="control-group  " >

                        <div class="controls">
                            <label class="checkbox">
                                <input type="checkbox" value="1" name="player[<?php echo $player->id;?>][is_active]" <?php if (isset($player->proj_year->team_id)) echo 'checked';?>>
                                Active
                            </label>
                        </div>
                    </div>



                    <div class="control-group " >

                        <div class="controls">
                            <select id="var_0" name="player[<?php echo $player->id;?>][team_id]" class="" >
                                <option value="">--</option>
                                <?php foreach ($teams as $team):?>
                                <?php if (isset($player->proj_year->team_id)) ?>
                                <?php $selected = (isset($player->proj_year->team_id) && $player->proj_year->team_id == $team->id) ? 'selected': '';?>
                                <?php //$selected = ($team->id == $player->team_id) ? 'selected' : '';?>
                                <option value="<?php echo $team->id;?>"  <?php echo $selected;?>><?php echo $team->short_name;?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>



            </td>
        </tr>
    <?php endforeach;?>
</tbody>
</table>

</div>
</div>
</form>

</div>
</div>
