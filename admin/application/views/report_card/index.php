    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-content nopadding">
                    <form class="form-horizontal" method="get" action="<?php echo current_url();?>">
                      <div class="control-group">
                        <label class="control-label" for="inputEmail">Position</label>
                        <div class="controls">
                          <select id="position_id" name="position_id">
                            <option value="">Select Position</option>
                            <?php foreach ($positions as $position):?>
                            <?php $selected = ($position->id == $position_info->id) ? 'selected' : '';?>
                            <option value="<?php echo $position->id;?>" <?php echo $selected ;?>><?php echo $position->name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn">Submit</button>
          </div>
      </div>
  </form>

</div>
</div>
</div>
</div>
<div class="row-fluid" id="response" style="display:none;">
  <div class="alert alert-success">
    <a href="#" data-dismiss="alert" class="close">×</a>
    SORT ORDER UPDATED
  </div>
</div>
<?php if ($stats):?>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-title">
                    Stats
                    <a href="<?php echo site_url('stat/add/'.$position_info->id);?>" class="btn btn-success btn-mini pull-right">Add Stat</a>

                </div>
                <div class="box-content nopadding">
                    <table class="table table-striped table-bordered" id="sortable">
                        <thead>
                            <tr>
                                <th>Position</th>
                                <th>Name</th>
                                <th>Short Name</th>
                                <th>Yearly Stat</th>
                                <th>Projection Stat</th>
                                <th style="width:210px;" >&nbsp;</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Position</th>
                                <th>Name</th>
                                <th>Short Name</th>
                                <th>Yearly Stat</th>
                                <th>Projection Stat</th>
                                <th style="width:210px;" >&nbsp;</th>
                            </tr>
                        </tfoot>
                        <tbody id="pages_index">
                            <?php foreach ($stats as $i => $stat):?>
                            <tr class="" id="row_<?php echo $stat->id;?>">
                                <td><?php echo FR_lookup_id('position_model', $stat->position_id, 'name');?></td>
                                <td><?php echo $stat->name;?></td>
                                <td><?php echo $stat->short_name;?></td>
                                <td><?php echo ($stat->yearly_stat == 1) ? 'Yes' : 'No';?></td>
                                <td><?php echo ($stat->projection_stat == 1) ? 'Yes' : 'No';?></td>
                     <!--
                        <td class="center"> 4</td>
                        <td class="center">X</td>
                    -->
                    <td class="">
                        <a href="<?php echo site_url('stat/edit/'.$stat->id);?>" class="btn btn-info btn-mini">Edit</a>
                        <a href="#" class="btn btn-danger btn-mini delete">Delete</a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
</div>
</div>
</div>
<script>
$(document).ready(function() {


function slideout(){
      setTimeout(function(){
        $("#response").slideUp("slow", function () {
        });

      }, 2000);}


     var fixHelper = function(e, ui) {
      ui.children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    };


    $("#sortable tbody").sortable({
      helper: fixHelper,
      opacity: 0.8,
      cursor: 'move',
      update: function() {
        var order = $(this).sortable("serialize") + '&update=update';
        //console.log(order);
        var url = '<?php echo site_url('/stat/sort');?>';
        //var url = '';
        $.post(url, order, function(theResponse){
          //$("#response").html(theResponse);
          $("#response").slideDown('slow');
          slideout();
          //alert(order);
        });
      }
    }).disableSelection();
});
</script>
<?php endif;?>
