    <div class="row-fluid">
      <div class="span12">

        <?php echo validation_errors(); ?>
        <div class="box">
          <div class="box-content nopadding">
            <form class="form-horizontal" method="post" action="<?php echo current_url();?>">
              <div class="control-group">
                <label class="control-label" for="api_key">API KEY</label>
                <div class="controls">
                  <input type="text" id="api_key" class="span10" placeholder="API KEY" name="api_key" value="<?php echo set_value('api_key', $api['key']);?>">
                </div>
              </div>

<?php /*
              <div class="control-group">
                <label class="control-label" for="endpoint">Endpoint</label>
                <div class="controls">
                  <input type="text" id="endpoint" class="span10" placeholder="Endpoint" name="endpoint" value="<?php echo set_value('endpoint', $api['endpoint']);?>">
                </div>
              </div>
*/?>

              <div class="control-group">
                <label class="control-label" for="endpoint">Endpoint</label>
                <div class="controls">
                  <select id="endpoint" name="endpoint">
                    <?php foreach ($avail_endpoints as $option_id => $option):?>
                    <?php //$selected = ($option_id == $stat_info->projection_stat) ? 'selected' : '';?>
                    <?php $selected = '';?>
                    <option value="<?php echo $option_id;?>"  <?php echo $selected;?>><?php echo $option;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>


            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">Submit</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>



  <div class="row-fluid">
    <div class="span12 well">
      <?php if (isset($response)):?>
      <?php $this->rest->debug();?>
      <?php var_dump($response);?>
    <?php endif;?>
  </div>
</div>

